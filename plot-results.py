import json
import os
import matplotlib.pyplot as plt
from pathlib import Path
import argparse
# Importing libraries
import numpy as np
import math
import re
from utils import bd_rate


default_font = {
    'size': 22,
}

def plot(stat, color, marker='.', linestyle='-', ax=None):
    x = stat['bpp']
    y = stat['PSNR']
    label = stat['name']
    module = ax or plt
    p = module.plot(x, y, label=label, linestyle=linestyle,
        marker=marker, markersize=10, linewidth=1.6, color=color
    )
    return p


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--results_dir',  type=str, default='results')
    parser.add_argument('--baseline_name',  type=str, default='scale_hyperprior_kodak')         # baseline w.r.t which BD-Rate is calculated
    parser.add_argument('--labels', type=str, nargs='+', default=None)  
    parser.add_argument('--colors', type=str, nargs='+', default=['blue', 'red', 'brown', 'orange', 'green', 'cyan', 'indigo']*100) 
    parser.add_argument('--markers', type=str, nargs='+', default=['.']*700) 
    parser.add_argument('--styles', type=str, nargs='+', default=['-']*700) 
    parser.add_argument('--label_size', type=int, default=13)      
    parser.add_argument('--xlim', type=float, nargs='+', default=[0, 1.5])          # change to fit plot in window
    parser.add_argument('--ylim', type=float, nargs='+', default=[24, 46])          # change to fit plot in window
    args = parser.parse_args()

    fig1, ax = plt.subplots(figsize=(8,8))

    results_dir = Path(args.results_dir)
    all_methods_results = []
    for fpath in results_dir.rglob('*.json'):
        # print(fpath)
        with open(fpath, 'r') as f:
            results = json.load(f)
        results['name'] = fpath.stem
        print(results['name'])
        if results['name'] == args.baseline_name:
            baseline_rate = results['bpp']
            baseline_psnr = results['PSNR']
        all_methods_results.append(results)
    all_methods_results = sorted(all_methods_results, key=lambda d: d['name'])

    colors = args.colors
    markers = args.markers
    styles = args.styles
    i = 0
    for results in all_methods_results:
        BD_RATE = bd_rate(baseline_rate, baseline_psnr, results['bpp'], results['PSNR'])
        print(f"{results['name']}: BD-rate = {BD_RATE}")
        plot(results, marker=markers[i], linestyle=styles[i], color=colors[i], ax=ax)
        i+=1

    plt.title('Rate-Distortion trade-off on KODAK', fontdict=default_font)
    plt.grid(True, alpha=0.32)
    handles, labels = plt.gca().get_legend_handles_labels()
    if args.labels is not None:
        order = [0, 1, 2, 3]
        labels = args.labels
        plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order], loc='lower right', prop={'size': args.label_size})
    else:
        plt.legend(loc='lower right', prop={'size': args.label_size})
    plt.xlabel('Bits per pixel (bpp)', fontdict=default_font)
    plt.ylabel('PSNR (dB)', fontdict=default_font)
    x_ticks = [(i) / 10 for i in range(0, 20, 1)]
    plt.xticks(x_ticks)
    y_ticks = [i for i in range(24, 37, 1)]
    plt.yticks(y_ticks)
    plt.xlim(args.xlim)
    plt.ylim(args.ylim)
    plt.tight_layout()
    plt.savefig(f"images/img.jpg")
    plt.show(block=True)


if __name__ == '__main__':
    main()
