from pathlib import Path
from tqdm import tqdm
from collections import defaultdict, OrderedDict
from PIL import Image
import os
import json
import time
import math
import argparse
import numpy as np
import torch
import torch.cuda.amp as amp
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.nn.functional as F

from torch.utils.data import Dataset, DataLoader
import torchvision as tv
from pytorch_msssim import ms_ssim
import timm
import timm.utils

from compressai.datasets import Vimeo90kDataset
from models.quantization import BIT_TYPE_DICT
from models.quantization.quant_layer import QuantModule
from models.quantization.quant_model import QuantModel
        

class ImageDataset(Dataset):
    def __init__(self, root, transform):
        self.root = root # will be accessed by the training script
        self.transform = transform
        # scan and add images
        self.image_paths = sorted(Path(root).rglob('*.*'))
        assert len(self.image_paths) > 0, f'Found {len(self.image_paths)} images in {root}.'

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, index):
        impath = self.image_paths[index]
        img = Image.open(impath).convert('RGB') 
        im = self.transform(img)
        return im


def get_trainloader(root_dir, img_size: int, batch_size: int, workers: int, distributed=False):
    """ get training data loader

    Args:
        root_dir ([type]): root directory containing training images
        img_size (int): input image size
        batch_size (int): batch size for training
        workers (int): number of cpu workers for data loading
        distributed (bool): whether distributed training is performed
    """
    transform = [
        tv.transforms.RandomCrop(img_size, pad_if_needed=True, padding_mode='reflect'),
        tv.transforms.RandomHorizontalFlip(p=0.5),
        tv.transforms.ToTensor(),
    ]
    transform = tv.transforms.Compose(transform)
    trainset = ImageDataset(root_dir, transform=transform)
    sampler = torch.utils.data.distributed.DistributedSampler(trainset) if distributed else None
    trainloader = torch.utils.data.DataLoader(
        trainset, batch_size=batch_size, shuffle=(sampler is None), num_workers=workers,
        pin_memory=False, drop_last=False, sampler=sampler
    )
    return trainloader


def get_valloader(root_dir, img_size=256, batch_size=1, workers=0, KODAK=False):
    """ get validation/test data loader

    Args:
        root_dir ([type]): root directory containing validation/test images
        img_size (int): input image size
        batch_size (int): batch size for validation
        workers (int): number of cpu workers for data loading
        KODAK (bool): True if KODAK dataset is being loaded
    """
    if KODAK:
        transform = [tv.transforms.ToTensor()]
    else:
        transform = [
            tv.transforms.CenterCrop(img_size),
            tv.transforms.ToTensor(),
        ]
    transform = tv.transforms.Compose(transform)
    dataset = ImageDataset(root_dir, transform=transform)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=batch_size, shuffle=False, num_workers=workers,
        pin_memory=True, drop_last=False
    )
    return dataloader


def compute_psnr(a, b):
    mse = F.mse_loss(a, b)
    return -10 * math.log10(mse)

def compute_msssim(a, b):
    return ms_ssim(a, b, data_range=1., win_size=3,  win_sigma=1).item()


def compute_loss(x, x_hat, likelihoods, bpp_lmb):
    nB, _, H, W = x.size()
    bpp_loss = sum(
            (-1.0 * torch.log2(likelihood).sum(dim=(1, 2, 3)) / float(H * W))
            for likelihood in likelihoods.values()
        )
    mse_loss = F.mse_loss(x, x_hat)
    loss = bpp_lmb*(255**2)*mse_loss + bpp_loss.mean()

    stats = OrderedDict()
    stats['loss'] = loss
    stats['bppix'] = bpp_loss.mean().item()
    stats['MSE'] = mse_loss.item()
    stats['PSNR'] = compute_psnr(x, x_hat)

    return stats


@torch.no_grad()
def evaluate_model(model: torch.nn.Module, testloader):
    """ Image classification evaluation with a testloader.

    Args:
        model (torch.nn.Module): pytorch model
        testloader (torch.utils.data.Dataloader): test dataloader
    """
    device = next(model.parameters()).device
    dtype = next(model.parameters()).dtype

    stats_avg_meter = defaultdict(timm.utils.AverageMeter)
    print(f'Evaluating {type(model)}, device={device}, dtype={dtype}')
    print(f'batch_size={testloader.batch_size}, num_workers={testloader.num_workers}')
    pbar = tqdm(testloader)
    
    for imgs in pbar:
        # sanity check
        imgs: torch.FloatTensor
        assert (imgs.dim() == 4)
        imgs = imgs.to(device=device, dtype=dtype)
        nB, _, imH, imW = imgs.shape
        if getattr(model, 'name', 0) == "cheng_anchor" or getattr(model.model, 'name', 0) == "cheng_anchor":
            p = 256  # maximum 6 strides of 2, and window size 4 for the smallest latent fmap: 4*2^6=256
            imgs = pad(imgs, p)
        # forward pass, get prediction
        out_dict = model(imgs)
        rec_imgs = out_dict["x_hat"]
        bpp_lmb = out_dict["lmb"]
        if getattr(model, 'name', 0) == "cheng_anchor" or getattr(model.model, 'name', 0) == "cheng_anchor":
            rec_imgs = crop(rec_imgs, (imH, imW))
            rec_imgs.clamp_(0, 1)
        stats = compute_loss(imgs, rec_imgs, out_dict["likelihoods"], bpp_lmb)
        for k, v in stats.items():
            stats_avg_meter[k].update(float(v), n=nB)
        # logging
        msg = ''.join([f'{k}={v.avg:.4g}, ' for k,v in stats_avg_meter.items()])            
        pbar.set_description(msg)                                                           
    pbar.close()

    # compute total statistics and return
    _random_key = list(stats_avg_meter.keys())[0]
    assert stats_avg_meter[_random_key].count == len(testloader.dataset)
    results = {k: v.avg for k,v in stats_avg_meter.items()}                         
    return results


def increment_dir(dir_root='runs/', name='exp'):
    """ Increament directory name. E.g., exp_1, exp_2, exp_3, ...

    Args:
        dir_root (str, optional): root directory. Defaults to 'runs/'.
        name (str, optional): dir prefix. Defaults to 'exp'.
    """
    assert isinstance(dir_root, (str, Path))
    dir_root = Path(dir_root)
    n = 0
    while (dir_root / f'{name}_{n}').is_dir():
        n += 1
    name = f'{name}_{n}'
    return name


class SimpleTable(OrderedDict):
    def __init__(self, init_keys=[]):
        super().__init__()
        # initialization: assign None to initial keys
        for key in init_keys:
            if not isinstance(key, str):
                print(f'Progress bar logger key: {key} is not a string')
            self[key] = None
        self._str_lengths = {k: 8 for k,v in self.items()}

    def _update_length(self, key, length):
        old = self._str_lengths.get(key, 0)
        if length <= old:
            return old
        else:
            self._str_lengths[key] = length
            return length

    def update(self, border=False):
        """ Update the string lengths, and return header and body

        Returns:
            str: table header
            str: table body
        """
        header = []
        body = []
        for k,v in self.items():
            # convert any object to string
            key = self.obj_to_str(k)
            val = self.obj_to_str(v)
            # get str length
            str_len = max(len(key), len(val)) + 2
            str_len = self._update_length(k, str_len)
            # make header and body string
            keystr = f'{key:^{str_len}}|'
            valstr = f'{val:^{str_len}}|'
            header.append(keystr)
            body.append(valstr)
        header = ''.join(header)
        if border:
            header = print(header)
        body = ''.join(body)
        return header, body

    def get_header(self, border=False):
        header = []
        body = []
        for k in self.keys():
            key = self.obj_to_str(k)
            str_len = self._str_lengths[k]
            keystr = f'{key:^{str_len}}|'
            header.append(keystr)
        header = ''.join(header)
        if border:
            header = print(header)
        return header

    def get_body(self):
        body = []
        for k,v in self.items():
            val = self.obj_to_str(v)
            str_len = self._str_lengths[k]
            valstr = f'{val:^{str_len}}|'
            body.append(valstr)
        body = ''.join(body)
        return body

    @staticmethod
    def obj_to_str(obj, digits=4):
        if isinstance(obj, str):
            return obj
        elif isinstance(obj, float) or hasattr(obj, 'float'):
            obj = float(obj)
            return f'{obj:.{digits}g}'
        elif isinstance(obj, list):
            strings = [SimpleTable.obj_to_str(item, 3) for item in obj]
            return '[' + ', '.join(strings) + ']'
        elif isinstance(obj, tuple):
            strings = [SimpleTable.obj_to_str(item, 3) for item in obj]
            return '(' + ', '.join(strings) + ')'
        else:
            return str(obj)


def get_cosine_lrf(n, lrf_min, T):
    """ Cosine learning rate factor

    Args:
        n (int): current epoch. 0, 1, 2, ..., T
        lrf_min (float): final (should also be minimum) learning rate factor
        T (int): total number of epochs
    """
    assert 0 <= n <= T, f'n={n}, T={T}'
    lrf = lrf_min + 0.5 * (1 - lrf_min) * (1 + math.cos(n * math.pi / T))
    return lrf


def bd_rate(r1, psnr1, r2, psnr2):
    """ Compute average bit rate saving of RD-2 over RD-1.

    Equivalent to the implementations in:
    https://github.com/Anserw/Bjontegaard_metric/blob/master/bjontegaard_metric.py
    https://github.com/google/compare-codecs/blob/master/lib/visual_metrics.py

    args:
        r1    (list, np.ndarray): baseline rate
        psnr1 (list, np.ndarray): baseline psnr
        r2    (list, np.ndarray): rate 2
        psnr2 (list, np.ndarray): psnr 2
    """
    lr1 = np.log(r1)
    lr2 = np.log(r2)

    # fit each curve by a polynomial
    degree = 2
    p1 = np.polyfit(psnr1, lr1, deg=degree)
    p2 = np.polyfit(psnr2, lr2, deg=degree)
    # compute integral of the polynomial
    p_int1 = np.polyint(p1)
    p_int2 = np.polyint(p2)
    # area under the curve = integral(max) - integral(min)
    min_psnr = max(min(psnr1), min(psnr2))
    max_psnr = min(max(psnr1), max(psnr2))
    auc1 = np.polyval(p_int1, max_psnr) - np.polyval(p_int1, min_psnr)
    auc2 = np.polyval(p_int2, max_psnr) - np.polyval(p_int2, min_psnr)

    # find avgerage difference
    avg_exp_diff = (auc2 - auc1) / (max_psnr - min_psnr)
    avg_diff = (np.exp(avg_exp_diff) - 1) * 100

    if False: # debug
        import matplotlib.pyplot as plt
        plt.figure(figsize=(8,6))
        l1 = plt.plot(psnr1, lr1, label='PSNR-logBPP 1',
                      marker='.', markersize=12, linestyle='None')
        l2 = plt.plot(psnr2, lr2, label='PSNR-logBPP 2',
                      marker='.', markersize=12, linestyle='None')
        x = np.linspace(min_psnr, max_psnr, num=100)
        # x = np.linspace(min_psnr-10, max_psnr+10, num=100)
        plt.plot(x, np.polyval(p1, x), label='polyfit 1',
                 linestyle='-', color=l1[0].get_color())
        plt.savefig("images/Exp10.jpg")
        plt.plot(x, np.polyval(p2, x), label='polyfit 2',
                 linestyle='-', color=l2[0].get_color())
        plt.savefig("images/Ex11.jpg")
        plt.legend(loc='lower right')
        plt.xlim(np.concatenate([psnr1,psnr2]).min()-1, np.concatenate([psnr1,psnr2]).max()+1)
        plt.ylim(np.concatenate([lr1, lr2]).min()-0.1, np.concatenate([lr1, lr2]).max()+0.1)
        # plt.ylim(np.concatenate(lr1, lr2).min(), max(np.concatenate(lr1, lr2)))
        plt.show()
    return avg_diff


def pad(x, p=2 ** 6):
    h, w = x.size(2), x.size(3)
    H = (h + p - 1) // p * p
    W = (w + p - 1) // p * p
    padding_left = (W - w) // 2
    padding_right = W - w - padding_left
    padding_top = (H - h) // 2
    padding_bottom = H - h - padding_top
    return F.pad(
        x,
        (padding_left, padding_right, padding_top, padding_bottom),
        mode="constant",
        value=0,
    )


def crop(x, size):
    H, W = x.size(2), x.size(3)
    h, w = size
    padding_left = (W - w) // 2
    padding_right = W - w - padding_left
    padding_top = (H - h) // 2
    padding_bottom = H - h - padding_top
    return F.pad(
        x,
        (-padding_left, -padding_right, -padding_top, -padding_bottom),
        mode="constant",
        value=0,
    )


def compute_padding(in_h: int, in_w: int, *, out_h=None, out_w=None, min_div=1):
    """Returns tuples for padding and unpadding.

    Args:
        in_h: Input height.
        in_w: Input width.
        out_h: Output height.
        out_w: Output width.
        min_div: Length that output dimensions should be divisible by.
    """
    if out_h is None:
        out_h = (in_h + min_div - 1) // min_div * min_div
    if out_w is None:
        out_w = (in_w + min_div - 1) // min_div * min_div

    if out_h % min_div != 0 or out_w % min_div != 0:
        raise ValueError(
            f"Padded output height and width are not divisible by min_div={min_div}."
        )

    left = (out_w - in_w) // 2
    right = out_w - in_w - left
    top = (out_h - in_h) // 2
    bottom = out_h - in_h - top

    pad = (left, right, top, bottom)
    unpad = (-left, -right, -top, -bottom)

    return pad, unpad


def calibrate(model, dataloader):
    assert isinstance(model, QuantModel)
    device = next(model.parameters()).device
    model.set_quant_state(False, False)
    model.set_calibrate(True, True)
    with torch.no_grad():
        for j, img, in enumerate(dataloader):
            img = img.to(device)
            if j == 10:
                break
            model(img)
        model(img)
    model.set_calibrate(False, False)
    

def get_model_size(model, model_type="full_precision", bit_per_module=None, pruned_model=False):
    assert model_type in ["full_precision", "fixed_precision_quantization", "mixed_precision_quantization"], '--model type not supported'
   
    if model_type == "full_precision":
        if isinstance(model, QuantModel):
            if pruned_model:
                model_size = 32.0 * float(sum(torch.count_nonzero(param) for _, param in model.model.named_parameters()))
            else:
                model_size = 32.0 * sum(param.numel() for _, param in model.model.named_parameters())
        else:
            if pruned_model:
                model_size = 32.0 * float(sum(torch.count_nonzero(param) for _, param in model.named_parameters()))
            else:
                model_size = 32.0 * sum(param.numel() for _, param in model.named_parameters())
    elif model_type == "fixed_precision_quantization":
        assert isinstance(bit_per_module, int)
        model_size = 0
        for name, param in model.model.named_parameters():
            if 'entropy_bottleneck' in name or 'quantizer' in name:
                model_size += 32 * param.numel()
            else:
                if pruned_model:
                    model_size += bit_per_module * float(torch.count_nonzero(param))
                else:
                    model_size += bit_per_module * param.numel()
    elif model_type == "mixed_precision_quantization":
        assert isinstance(bit_per_module, list)
        model_size = 0
        for name, param in model.model.named_parameters():
            if 'entropy_bottleneck' in name or 'quantizer' in name:
                model_size += 32 * param.numel()
        modules_to_quant = []
        for name, module in model.named_modules():
            if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                modules_to_quant.append(module)
        assert len(modules_to_quant)==len(bit_per_module), '--Mismatch in the number of layers to be quantized'
        for module, bit_type_str in zip(modules_to_quant, bit_per_module): 
            if pruned_model:
                model_size += BIT_TYPE_DICT[bit_type_str].bits * float(torch.count_nonzero(module.weight)) 
                model_size += BIT_TYPE_DICT[bit_type_str].bits * float(torch.count_nonzero(module.bias)) 
            else:
                model_size += BIT_TYPE_DICT[bit_type_str].bits * module.weight.numel() 
                model_size += BIT_TYPE_DICT[bit_type_str].bits * module.bias.numel() 
    
    return model_size * 10**-6 / 8.0 


def get_beta(model, beta, beta_adjust, bit_per_module, target_CR, CR_dict, log_dir, verbose=False):
    """
    Args:
        model (QuantModel): quantized model
        bit_per_module (list): list of bit-precisions for each layer
        target_CR (float): target compression ratio of FMPQ model w.r.t fixed-precision quantized model
        CR_dict (dict): logging dict for achieved CR and threshold beta
        log_dir (str): root-dir for storing CR_dict
    """
    
    fixed_quant_model_size = get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8)
    model_size = get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=[B.name for B in bit_per_module])
    CR_current = model_size/fixed_quant_model_size
    
    converge = False
    
    if np.abs(CR_current - target_CR) <= 0.01:         
        if verbose:
            print(f"Compression ratio: {CR_current}")
            print(f"Target compression ratio has been achieved with a threshold value of {beta}.")
            CR_dict[beta] = f"CR={float(round(CR_current, 3))},   beta_adjust={beta_adjust}"                               # (float(round(CR_current, 3)), beta_adjust)
            json.dump(CR_dict, fp=open(log_dir / 'CR_beta.json', 'w'), indent=2)
        converge = True
    elif CR_current <= target_CR:
        if verbose:
            print(f"Compression ratio: {CR_current}")
            print(f"Target compression ratio has not been achieved with a threshold value of {beta}.")
            CR_dict[beta] = f"CR={float(round(CR_current, 3))},   beta_adjust={beta_adjust}"                           # (float(round(CR_current, 3)), beta_adjust)
            json.dump(CR_dict, fp=open(log_dir / 'CR_beta.json', 'w'), indent=2)
        beta -= beta_adjust
        beta = np.max((beta, 0.001))
        beta_adjust *= 0.1
        beta += beta_adjust
        if verbose:
            print(f"Threshold value is increased to {beta}.")
    else:
        if np.abs(CR_current- target_CR) >= .25:
            beta_adjust *= 5
        elif np.abs(CR_current- target_CR) >= .10:
            beta_adjust *= 2
        if verbose:
            print(f"Compression ratio: {CR_current}")
            CR_dict[beta] = f"CR={float(round(CR_current, 3))},   beta_adjust={beta_adjust}"                               # (float(round(CR_current, 3)), beta_adjust)
            json.dump(CR_dict, fp=open(log_dir / 'CR_beta.json', 'w'), indent=2)
            print(f"Target compression ratio has not been achieved with a threshold value of {beta}.")
        beta += beta_adjust
        if verbose:
            print(f"Threshold value is increased to {beta}.")
            
    return beta, beta_adjust, CR_dict, converge


def quant_stats(model, log_dir, quality):
    with open(os.path.join(log_dir, 'stats.txt'), 'a') as f:
        f.write(f"Model Quality: {quality}" + '\n')
    for name, module in model.named_modules():
        if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
            weight = module.weight_quantizer(module.weight)
            if module.org_module_type == "ConvTranspose2d":
                num_weight = torch.unique(weight[:,0,:,:]).reshape(-1).shape
                num_weight_before = torch.unique(module.weight[:,0,:,:]).reshape(-1).shape
            else:
                num_weight = torch.unique(weight[0]).reshape(-1).shape
                num_weight_before = torch.unique(module.weight[0]).reshape(-1).shape
            msg = f'Module: {name},     type: {module.org_module_type},     bit-type: {module.weight_quantizer.bit_type.name},      unique weight elements before: {num_weight_before},     unique weight elements after: {num_weight}'
            print(msg)
            with open(os.path.join(log_dir, 'quant_stats.txt'), 'a') as f:
                f.write(msg + '\n')
                

