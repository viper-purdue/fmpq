from pathlib import Path
from tqdm import tqdm
from collections import defaultdict, OrderedDict
from PIL import Image
import os
import sys
import json
import time
import math
import argparse
import numpy as np
import random
import torch
import torch.cuda.amp as amp
import torch.nn.functional as tnf
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.optim as optim
from torch.nn.utils import prune
from torch.hub import load_state_dict_from_url
import compressai

import torchvision as tv
import timm
import timm.utils
from pytorch_msssim import ms_ssim

from compressai.datasets import Vimeo90kDataset
from compressai.zoo.pretrained import load_pretrained

from utils import get_trainloader, get_valloader, ImageDataset, compute_loss, evaluate_model, increment_dir, SimpleTable, get_cosine_lrf, pad, crop, calibrate, get_model_size, get_beta, quant_stats
from models.vae import FactorizedPrior, ScaleHyperprior, Cheng2020Anchor, model_urls, cfgs
from models.registry import get_model
from models.quantization import BIT_TYPE_DICT
from models.quantization.quant_layer import QuantModule
from models.quantization.quant_model import QuantModel                                    


def get_config():
    # ====== set the run settings ======
    parser = argparse.ArgumentParser()
    # wandb setting
    parser.add_argument('--wbproject',  type=str,  default='model-compression')
    parser.add_argument('--wbgroup',    type=str,  default='quantization')
    parser.add_argument('--wbmode',     type=str,  default='disabled')
    # model setting
    parser.add_argument('--model',      type=str,  default='cheng_anchor_quantized')
    parser.add_argument('--model_quality', type=int,  default='1')
    # resume setting
    parser.add_argument('--resume',     type=str,  default='')
    parser.add_argument('--pretrained_compressai',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--pretrained',   action=argparse.BooleanOptionalAction, default=False)
    #quanitzation settings
    parser.add_argument('--quantize_model',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--wquant_calibration_mode', type=str, default="channel_wise", choices=['layer_wise', 'channel_wise'])
    parser.add_argument('--wquantizer_type', type=str, default="scaled-lsq", choices=['lsq', 'uniform', 'scaled-lsq'])
    parser.add_argument('--wquant_scale_method', type=str, default="minmax", choices=['minmax', 'ema', 'omse'])
    parser.add_argument('--aquant_calibration_mode', type=str, default="channel_wise", choices=['layer_wise', 'channel_wise'])
    parser.add_argument('--aquantizer_type', type=str, default="scaled-lsq", choices=['lsq', 'uniform', 'scaled-lsq'])
    parser.add_argument('--aquant_scale_method', type=str, default="minmax", choices=['minmax', 'ema', 'omse'])
    parser.add_argument('--wquant_bits', type=str, default="uint8")
    parser.add_argument('--aquant_bits', type=str, default="uint8")
    parser.add_argument('--dynamic_aquant',   action=argparse.BooleanOptionalAction, default=False)                                                         
    parser.add_argument('--MPQ',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--MPQ_threshold', type=float, default=2)
    parser.add_argument('--CR', type=float, default=0.6)
    parser.add_argument('--optim_params', type=str, default='qparams_netparams', choices=['qparams', 'netparams', 'qparams_netparams'])
    parser.add_argument('--qlr',         type=float,default=1e-4) 
    # training setting
    parser.add_argument('--root_dir', type=str, default='/home/jackfruit/a/hossai34/datasets/coco/images')
    parser.add_argument('--train_size', type=int,  default=256)
    parser.add_argument('--gpu_id', type=int, default=0)
    # evaluation setting
    parser.add_argument('--val_size',   type=int,  default=256)
    # optimization setting
    parser.add_argument('--batch_size', type=int,  default=16)
    parser.add_argument('--accum_num',  type=int,  default=1)
    parser.add_argument('--optimizer',  type=str,  default='adam', choices=['adam', 'sgd'])
    parser.add_argument('--lr',         type=float,default=1e-5)            # 0.01   # 1e-4     # 1e-5 for finetuning
    parser.add_argument('--lr_sched',   type=str,  default='cosine')                # 'cosine', 'plateau'
    # training policy setting
    parser.add_argument('--epochs',     type=int,  default=60)
    parser.add_argument('--amp',        action=argparse.BooleanOptionalAction, default=False)               
    # miscellaneous training setting
    parser.add_argument('--eval_first', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--eval_per',   type=int,  default=1)
    # device setting
    parser.add_argument('--fixseed',    action='store_true')
    parser.add_argument('--workers',    type=int,  default=4)
    parser.add_argument('--ddp_find',   action=argparse.BooleanOptionalAction, default=False)
    cfg = parser.parse_args()

    # optimizer
    cfg.momentum = 0.9
    return cfg


class TrainWrapper():
    def __init__(self) -> None:
        pass

    def set_device_(self):
        cfg = self.cfg

        local_rank = int(os.environ.get('LOCAL_RANK', -1))
        world_size = int(os.environ.get('WORLD_SIZE', 1))
        _count = torch.cuda.device_count()

        if world_size == 1: # standard mode
            assert local_rank == -1
            print(f'Visible devices={_count}, using idx 0:', torch.cuda.get_device_properties(0), '\n')
            device = torch.device('cuda', cfg.gpu_id)
            # device = 'cpu'
            is_main = True
            distributed = False
        else: # DDP mode
            assert local_rank >= 0
            assert torch.distributed.is_nccl_available()
            torch.distributed.init_process_group(backend="nccl")
            assert local_rank == torch.distributed.get_rank()
            assert world_size == torch.distributed.get_world_size()

            torch.distributed.barrier()
            device = torch.device('cuda', local_rank)
            is_main = (local_rank == 0)
            distributed = True

        if cfg.fixseed: # fix random seeds for reproducibility
            timm.utils.random_seed(2 + local_rank)
        torch.backends.cudnn.benchmark = True

        if is_main:
            print(f'Batch size on each dataloader (ie, GPU) = {cfg.batch_size}')
            print(f'Gradient accmulation: {cfg.accum_num} backwards() -> one step()')
            bs_effective = cfg.batch_size * world_size * cfg.accum_num
            msg = f'Effective batch size = {bs_effective}, learning rate = {cfg.lr}, ' 
            print(msg)

        cfg.world_size   = world_size
        self.device      = device
        self.local_rank  = local_rank
        self.is_main     = is_main
        self.distributed = distributed

    def set_dataset_(self):
        cfg = self.cfg

        if self.is_main:
            print('Initializing Datasets and Dataloaders...')
        train_split = 'train2017'
        val_split = 'val2017'
        calib_split = 'calib2017'
        DATA_DIR = Path(cfg.root_dir) 

        train_dir = os.path.join(DATA_DIR, train_split)
        val_dir = os.path.join(DATA_DIR, val_split)
        calib_dir = os.path.join(DATA_DIR, calib_split)

        trainloader = get_trainloader(root_dir=train_dir, img_size=cfg.train_size, batch_size=cfg.batch_size, workers=cfg.workers, distributed=self.distributed)
        quantloader = get_valloader(root_dir=calib_dir, img_size=cfg.val_size, batch_size=32, workers=cfg.workers//2)

        if self.is_main: # test set
            print(f'Training root: {train_dir}')
            print('Training transform:', trainloader.dataset.transform)
            valloader = get_valloader(root_dir=val_dir, img_size=cfg.val_size, batch_size=cfg.batch_size//4, workers=cfg.workers//2)
            print(f'Val root: {val_dir}')       
        else:
            valloader = None

        self.trainloader = trainloader
        self.valloader  = valloader
        self.quantloader = quantloader


    def set_model_(self):
        cfg = self.cfg
        
        if 'quantized' in cfg.model:
            name = '_'.join(cfg.model.split('_')[:-1])
        else:
            name = cfg.model
        (N, M), bpp_lmb = cfgs[name][cfg.model_quality]
        model_func = get_model(cfg.model)
        if not cfg.quantize_model:
            model = model_func(N=N, M=M, bpp_lmb=bpp_lmb)  
            if cfg.pretrained_compressai:
                model_url = model_urls[model.name]["mse"][(cfg.model_quality, (N, M), bpp_lmb)]
                state_dict = load_state_dict_from_url(model_url, progress=True)
                state_dict = load_pretrained(state_dict)
                model.load_state_dict(state_dict, strict=True)   
            elif cfg.pretrained:
                checkpoint_root = Path(f'checkpoints/{cfg.model}')
                checkpoint_paths = list(checkpoint_root.rglob('*.pt'))
                checkpoint_paths.sort()
                ckptpath = checkpoint_paths[cfg.model_quality-1]
                checkpoint = torch.load(ckptpath)
                model.load_state_dict(checkpoint['model'], strict=True)
            self.model = model.to(self.device)                  
        else:
            wq_params = {'scale_method': cfg.wquant_scale_method, 
                         'bit_type': BIT_TYPE_DICT[cfg.wquant_bits], 
                         'calibration_mode': cfg.wquant_calibration_mode, 
                         'quantizer_type': cfg.wquantizer_type}
            aq_params = {'scale_method': cfg.aquant_scale_method, 
                         'bit_type': BIT_TYPE_DICT[cfg.aquant_bits], 
                         'calibration_mode': cfg.aquant_calibration_mode, 
                         'quantizer_type': cfg.aquantizer_type, 
                         'dynamic': cfg.dynamic_aquant}
            model = model_func(N, M, bpp_lmb, wq_params, aq_params, cfg.model_quality, device=self.device, pretrained=cfg.pretrained)
            self.model = model.to(self.device)
        
        if self.distributed: # DDP mode
            self.model = DDP(model, device_ids=[self.local_rank], output_device=self.local_rank,
                             find_unused_parameters=cfg.ddp_find)


    def set_optimizer_(self):
        cfg, model = self.cfg, self.model
        
        if cfg.quantize_model:
            if cfg.optim_params == "qparams_netparams":
                for name, param in model.named_parameters():
                    param.requires_grad = True
            elif cfg.optim_params == "qparams":
                for name, param in model.named_parameters():
                    param.requires_grad = True
                    if "quantizer" not in name:
                        param.requires_grad = False
            elif cfg.optim_params == "netparams":
                for name, param in model.named_parameters():
                    param.requires_grad = True
                    if "quantizer" in name:
                        param.requires_grad = False
        else:
            for name, param in model.named_parameters():
                param.requires_grad = True

        pg_info = defaultdict(list)
        for k, v in model.named_parameters():
            assert isinstance(k, str) and isinstance(v, torch.Tensor)
            pg_info['weights'].append(f'{k:<80s} {v.shape}')

        qparams = [p for n, p in model.named_parameters() if "quantizer" in n]
        if self.is_main:
            print(f'Number of quantization parameters:  {len(qparams)}')
        netparams = [p for n, p in model.named_parameters() if "quantizer" not in n]
        if self.is_main:
            print(f'Number of network parameters:  {len(netparams)}')

        # optimizer
        if cfg.optimizer == 'sgd':
            if cfg.optim_params == "qparams_netparams":
                optimizer = torch.optim.SGD([{'params': netparams},{'params': qparams, 'lr': cfg.qlr}], lr=cfg.lr, momentum=cfg.momentum)
            elif cfg.optim_params == "qparams":
                cfg.lr = cfg.qlr
                optimizer = torch.optim.SGD(qparams, lr=cfg.lr, momentum=cfg.momentum)
            else:
                optimizer = torch.optim.SGD(netparams, lr=cfg.lr, momentum=cfg.momentum)
        elif cfg.optimizer == 'adam':
            if cfg.optim_params == "qparams_netparams":
                optimizer = torch.optim.Adam([{'params': netparams},{'params': qparams, 'lr': cfg.qlr}], lr=cfg.lr)
            elif cfg.optim_params == "qparams":
                cfg.lr = cfg.qlr
                optimizer = torch.optim.Adam(qparams, lr=cfg.lr)
            else:
                optimizer = torch.optim.Adam(netparams, lr=cfg.lr)
        else:
            raise ValueError(f'Unknown optimizer: {cfg.optimizer}')

        if self.is_main:
            print('optimizer parameter groups:', *[f'[{k}: {len(pg)}]' for k, pg in pg_info.items()])
            self.pg_info_to_log = pg_info
            print()

        self.optimizer = optimizer
        self.scaler = amp.GradScaler(enabled=cfg.amp) # Automatic mixed precision
        if cfg.lr_sched == 'plateau':
            self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, mode='min', factor=0.1, patience=5)

    def set_logging_dir_(self):
        cfg = self.cfg

        prev_loss = 1e8
        log_parent = Path(f'runs/{cfg.wbproject}')
        if cfg.resume: # resume                                                 # For the case of resume with MPQ, need to load appropriate bits for each layer i.e init_MPQ
            model = self.model
            if self.distributed:
                model = timm.utils.unwrap_model(model)
            run_name = cfg.resume
            self.run_name = run_name
            log_dir = log_parent / run_name
            assert log_dir.is_dir(), f'Try to resume from {log_dir} but it does not exist'
            if cfg.MPQ:
                with open(os.path.join(log_dir, f'bit_types.json'), 'r') as f:
                    bit_type_names = json.load(f) 
                modules_to_quant = []
                module_names = []
                for name, module in model.named_modules():
                    if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                        modules_to_quant.append(module)
                        module_names.append(name)
                last_module_name = module_names[-1]
                assert len(modules_to_quant) == len(bit_type_names), '--Mismatch in the number of layers to be quantized'
                for i, (module, bit_type_str) in enumerate(zip(modules_to_quant, bit_type_names.values())): 
                    if module_names[i] == last_module_name:
                        module.init_MPQ(BIT_TYPE_DICT[bit_type_str], act_bit_type=BIT_TYPE_DICT['uint8'])
                    else:
                        module.init_MPQ(BIT_TYPE_DICT[bit_type_str])
            calibrate(model, self.quantloader)
            model.set_quant_state(True, False)
            ckpt_path = log_dir / 'last.pt'
            checkpoint = torch.load(ckpt_path)
            if self.distributed:
                self.model.module.load_state_dict(checkpoint['model'])
            else:
                self.model.load_state_dict(checkpoint['model'])
            self.optimizer.load_state_dict(checkpoint['optimizer'])
            if cfg.amp:
                self.scaler.load_state_dict(checkpoint['scaler'])
            if cfg.lr_sched == 'plateau':
                self.scheduler.load_state_dict(checkpoint["lr_scheduler"])
            start_epoch = checkpoint['epoch']
            prev_result = checkpoint.get('results', None)
            prev_loss = prev_result['loss'] if prev_result is not None else prev_loss
            if self.is_main:
                print(f'Resuming run {log_dir}. Loaded checkpoint from {ckpt_path}.',
                      f'Epoch={start_epoch}, results={prev_result}')
        elif cfg.quantize_model:
            if cfg.MPQ:
                _base = f'{cfg.model}-quality_{cfg.model_quality}-optim_params_{cfg.optim_params}-MPQ-quantized'
            else:
                _base = f'{cfg.model}-quality_{cfg.model_quality}-optim_params_{cfg.optim_params}-quantized'
            run_name = increment_dir(dir_root=log_parent, name=_base)
            self.run_name = run_name
            log_dir = log_parent / run_name
            if self.is_main:
                print(f"MODEL WILL BE SAVED AS: {run_name}")
                os.makedirs(log_dir, exist_ok=False)
                print(str(self.model), file=open(log_dir / 'model.txt', 'w'))
                json.dump(cfg.__dict__, fp=open(log_dir / 'config.json', 'w'), indent=2)
                json.dump(self.pg_info_to_log, fp=open(log_dir / 'optimizer.json', 'w'), indent=2)
                print('Training config:\n', cfg, '\n')
            start_epoch = 0
        else: # new experiment
            _base = f'{cfg.model}-quality_{cfg.model_quality}'
            run_name = increment_dir(dir_root=log_parent, name=_base)
            log_dir = log_parent / run_name # logging dir
            self.run_name = run_name
            if self.is_main:
                print(f"MODEL WILL BE SAVED AS: {run_name}")
                os.makedirs(log_dir, exist_ok=False)
                print(str(self.model), file=open(log_dir / 'model.txt', 'w'))
                json.dump(cfg.__dict__, fp=open(log_dir / 'config.json', 'w'), indent=2)
                json.dump(self.pg_info_to_log, fp=open(log_dir / 'optimizer.json', 'w'), indent=2)
                print('Training config:\n', cfg, '\n')
            start_epoch = 0

        if hasattr(self.model, "bpp_lmb") and self.is_main:
            print(f"Model is training with lambda value: {self.model.bpp_lmb}")
        cfg.log_dir = str(log_dir)
        self._log_dir     = log_dir
        self._start_epoch = start_epoch
        self._best_loss   = prev_loss
    
    def quantize_model_(self):
        cfg = self.cfg
        model = self.model
        if self.distributed:
            model = timm.utils.unwrap_model(model) 
        model.eval()
        model.set_quant_state(False, False)
        if self.is_main:
            results = self.evaluate(-1, -1)                                 # Here
            print('Results before quantization:', results)
        # Quantization
        start_time = time.time()
        calibrate(model, self.quantloader)
        model.set_quant_state(True, False)
        if self.is_main:
            results = self.evaluate(-1, -1)                                 # Here
            print('Results after weight quantization without optimization:', results)
        if cfg.dynamic_aquant:
            model.set_quant_state(True, False)
        else:
            model.set_quant_state(True, True)
        if cfg.optim_params == "qparams_netparams" or cfg.optim_params == "qparams":
            for name, param in model.named_parameters():
                if "quantizer" in name:
                    param.requires_grad = True
        end_time = time.time()
        if self.is_main:
            print(f"Total time to quantize model: {int((end_time-start_time)//3600)} hour(s) {round((end_time-start_time)/60 - float((end_time-start_time)//3600)*60)} minute(s)")
        
    def quantize_MPQ_(self):
        cfg = self.cfg        
        model = self.model 
        if self.distributed:
            model = timm.utils.unwrap_model(model) 
        model.eval()
        beta = cfg.MPQ_threshold
        CR_dict = {}
        beta_adjust = 1
        if self.is_main:
            start_time = time.time()
        while True:
            # Calibrate model quant parameters
            calibrate(model, self.quantloader)
            # Results before quantization
            results = evaluate_model(model, self.quantloader)
            loss = results['loss']
            if self.is_main:
                print(f"Results before quantization {results}")
            module_list = []        # all the Quantmodules
            name_list = []          # names of corresponding Quantmodules
            for name, module in model.named_modules():
                if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                    module_list.append(module)
                    name_list.append(name)
            last_module_name = name_list[-1]
            bit_per_module = [BIT_TYPE_DICT["uint10"]] * len(module_list)       # bit_type(class) of each Quantmodule of model initialized to uint10 for all modules
            bit_type_list = ['uint10', 'uint9', 'uint8', 'uint7', 'uint6', 'uint5', 'uint4', 'uint3', 'uint2']          # list of available bit_precisions
            if self.is_main:
                print(f"Number of quant modules {len(module_list)}")    
            for i in range(len(module_list)):
                modules_to_quant = module_list[:i+1]
                if self.is_main:
                    print(f"Step {i}/{len(module_list)} - Module being Quantized: {name_list[i]}; Type of module: {module_list[i].org_module_type}")
                prev_bit_type = BIT_TYPE_DICT["uint10"]
                # print([B.name for B in bit_per_module])
                for bit_str in bit_type_list:
                    bit_type = BIT_TYPE_DICT[bit_str]
                    bit_per_module[i] = bit_type                    
                    for module, B in zip(modules_to_quant, bit_per_module):
                        module.init_MPQ(B)
                    calibrate(model, self.quantloader)
                    modules_to_quant[-1].set_quant_state(True, False)
                    results = evaluate_model(model, self.quantloader)
                    quant_loss = results['loss']
                    delta_loss = (np.abs(loss-quant_loss)/loss) * 100
                    # print(f"Loss after quantization {results}")
                    if self.is_main:
                        print(f'Bit width {bit_str} Percentage change in loss {delta_loss}')                    
                    if delta_loss > beta:
                        if self.is_main:
                            print(f"Bit-type for {name_list[i]}: {prev_bit_type.name}")
                        # loss = prev_quant_loss
                        bit_per_module[i] = prev_bit_type
                        break
                    prev_bit_type = bit_type
                    # prev_quant_loss = quant_loss
            if self.is_main:
                print("Finished assigning bit-widhts")
                print([B.name for B in bit_per_module])
            ############################################## Adaptive Search ##################################################################     
            beta, beta_adjust, CR_dict, converge = get_beta(model, beta, beta_adjust, bit_per_module, cfg.CR, CR_dict, self._log_dir, verbose=self.is_main)
            if converge:
                break
            ############################################## Adaptive Search ##################################################################
        # assign appropriate bits to each layer
        for i, (module, B) in enumerate(zip(modules_to_quant, bit_per_module)):
            if name_list[i] == last_module_name:
                module.init_MPQ(B, act_bit_type=BIT_TYPE_DICT['uint8'])
            else:
                module.init_MPQ(B)
        # Calibrate model quant parameters
        calibrate(model, self.quantloader)
        if cfg.dynamic_aquant:
            model.set_quant_state(True, False)
        else:
            model.set_quant_state(True, True)
        if self.is_main:
            results = self.evaluate(-1, -1)                                 # Here
            print('Results after weight quantization without optimization:', results)
        if cfg.optim_params == "qparams_netparams" or cfg.optim_params == "qparams":
            for name, param in model.named_parameters():
                if "quantizer" in name:
                    param.requires_grad = True
        # save bit-assignments to each layer for later evaluation
        if self.is_main:
            bit_dict = {}
            for i, B in enumerate(bit_per_module):
                bit_dict[i] = B.name
            json.dump(bit_dict, fp=open(self._log_dir / 'bit_types.json', 'w'), indent=2)
            end_time = time.time()
            print(f"Total time to quantize model: {int((end_time-start_time)//3600)} hour(s) {round((end_time-start_time)/60 - float((end_time-start_time)//3600)*60)} minute(s)")

    def set_wandb_(self):
        cfg = self.cfg

        # check if there is a previous run to resume
        wbid_path = self._log_dir / 'wandb_id.txt'
        if os.path.exists(wbid_path):
            run_ids = open(wbid_path, mode='r').read().strip().split('\n')
            rid = run_ids[-1]
        else:
            rid = None
        # initialize wandb
        import wandb
        run_name = self.run_name
        wbrun = wandb.init(project=cfg.wbproject, group=cfg.wbgroup, name=run_name,
                           config=cfg, dir='runs/', id=rid, resume='allow',
                           save_code=True, mode=cfg.wbmode)
        cfg = wbrun.config
        cfg.wandb_id = wbrun.id
        with open(wbid_path, mode='a') as f:
            print(wbrun.id, file=f)

        self.wbrun = wbrun
        self.cfg = cfg
        
    
    def seed_all(self, seed=1029):
        random.seed(seed)
        os.environ['PYTHONHASHSEED'] = str(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
        torch.backends.cudnn.benchmark = False  # may slow
        torch.backends.cudnn.deterministic = True

    def main(self):
        # config
        self.seed_all()
        self.cfg = get_config()
        cfg = self.cfg

        # core
        self.set_device_()
        self.set_dataset_()
        self.set_model_()
        model = self.model
        self.set_optimizer_()
        # logging
        self.set_logging_dir_()
        if not cfg.resume:
            if cfg.quantize_model:
                if cfg.MPQ:
                    self.quantize_MPQ_()
                else:
                    self.quantize_model_()
                self.set_optimizer_()
        if self.is_main:
            self.set_wandb_()
            self.stats_table = SimpleTable(['Epoch', 'GPU_mem', 'lr'])
            
        if self.is_main:
            print(f'Number of trainable parameters: {len([p for n, p in model.named_parameters() if p.requires_grad])}')
        
        if cfg.quantize_model and self.is_main:
            quant_stats(model, self._log_dir, cfg.model_quality)

        # ======================== start training ========================
        for epoch in range(self._start_epoch, cfg.epochs):
            time.sleep(0.1)

            if self.distributed:
                self.trainloader.sampler.set_epoch(epoch)
            if hasattr(timm.utils.unwrap_model(self.model), 'set_epoch'):                  
                timm.utils.unwrap_model(self.model).set_epoch(epoch, cfg.epochs, verbose=self.is_main)

            pbar = enumerate(self.trainloader)
            if self.is_main:
                if ((epoch != self._start_epoch) or cfg.eval_first) and (epoch % cfg.eval_per == 0):
                    self.evaluate(epoch, niter=epoch*len(self.trainloader))

                self.init_logging_()
                pbar = tqdm(pbar, total=len(self.trainloader))

            if cfg.lr_sched == 'cosine' or cfg.lr_sched == 'plateau':
                self.adjust_lr_(epoch)                                                              
                
            timm.utils.unwrap_model(model).train()
            # model.train()
            for bi, imgs in pbar:
                niter = epoch * len(self.trainloader) + bi
                imgs = imgs.to(device=self.device)
                nB, _, imH, imW = imgs.shape
                
                if "cheng_anchor" in cfg.model:
                    p = 256  # maximum 6 strides of 2, and window size 4 for the smallest latent fmap: 4*2^6=256
                    imgs = pad(imgs, p)
                # forward
                with amp.autocast(enabled=cfg.amp):
                    out_dict = model(imgs)
                    rec_imgs = out_dict["x_hat"]
                    if "cheng_anchor" in cfg.model:
                        rec_imgs = crop(rec_imgs, (imH, imW))
                        rec_imgs.clamp_(0, 1)
                    bpp_lmb = out_dict["lmb"]
                    stats = compute_loss(imgs, rec_imgs, out_dict["likelihoods"], bpp_lmb)
                    loss = stats["loss"]
                    loss = loss / float(cfg.accum_num)
                self.scaler.scale(loss).backward()
                if cfg.quantize_model:    
                    torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
                # gradient averaged between devices in DDP mode
                self.scaler.step(self.optimizer)
                self.scaler.update()
                self.optimizer.zero_grad()

                if self.is_main:
                    self.logging(pbar, epoch, bi, niter, imgs, stats)
            if self.is_main:
                pbar.close()

        if self.is_main: 
            if cfg.quantize_model:
                quant_stats(model, self._log_dir, cfg.model_quality)               

            results = self.evaluate(epoch+1, niter)                                
            print('Training finished. results:', results)
        if self.distributed:
            torch.distributed.destroy_process_group()
        

    def adjust_lr_(self, epoch, loss=0):
        cfg = self.cfg
        if cfg.lr_sched == 'cosine':
            lrf = get_cosine_lrf(epoch, cfg.lr, cfg.epochs-1)
            qlrf = get_cosine_lrf(epoch, cfg.qlr, cfg.epochs-1)
            if cfg.optim_params == "qparams_netparams":
                self.optimizer.param_groups[0]['lr'] = cfg.lr * lrf
                self.optimizer.param_groups[1]['lr'] = cfg.qlr * qlrf
            else:
                self.optimizer.param_groups[0]['lr'] = cfg.lr * lrf
        elif cfg.lr_sched == 'plateau':
            self.scheduler.step(loss)


    def init_logging_(self):
        # initialize stats table and progress bar
        for k in self.stats_table.keys():
            self.stats_table[k] = 0.0
        self._pbar_header = self.stats_table.get_header()
        print('\n', self._pbar_header)
        time.sleep(0.1)

    @torch.no_grad()
    def logging(self, pbar, epoch, bi, niter, imgs, stats):
        cfg = self.cfg

        self.stats_table['Epoch'] = f'{epoch}/{cfg.epochs-1}'

        mem = torch.cuda.max_memory_allocated(self.device) / 1e9
        torch.cuda.reset_peak_memory_stats()
        self.stats_table['GPU_mem'] = f'{mem:.3g}G'

        cur_lr = self.optimizer.param_groups[0]['lr']
        self.stats_table['lr'] = cur_lr

        keys_to_log = []
        for k, v in stats.items():
            if isinstance(v, torch.Tensor) and v.numel() == 1:
                v = v.detach().cpu().item()
            assert isinstance(v, (float, int))
            prev = self.stats_table.get(k, 0.0)
            self.stats_table[k] = (prev * bi + v) / (bi + 1)
            keys_to_log.append(k)
        pbar_header, pbar_body = self.stats_table.update()
        if pbar_header != self._pbar_header:
            print(pbar_header, f"GPU: {self.local_rank}")
            self._pbar_header = pbar_header
        pbar.set_description(pbar_body)

        # # Weights & Biases logging
        if niter % 100 == 0:
            _num = min(16, imgs.shape[0])
            _log_dic = {
                'general/lr': cur_lr            }
            _log_dic.update(
                {'train/'+k: self.stats_table[k] for k in keys_to_log}
            )
            self.wbrun.log(_log_dic, step=niter)


    def evaluate(self, epoch, niter):
        cfg = self.cfg
        assert self.is_main
        # Evaluation
        _log_dic = {'general/epoch': epoch}
        _eval_model = timm.utils.unwrap_model(self.model).eval()
        results = evaluate_model(_eval_model, testloader=self.valloader)
        _log_dic.update({'metric/plain_val_'+k: v for k,v in results.items()})
        if epoch != -1:
            # save last checkpoint
            checkpoint = {
                'model'     : _eval_model.state_dict(),
                'optimizer' : self.optimizer.state_dict(),
                'scaler'    : self.scaler.state_dict(),
                'epoch'     : epoch,
                'results'   : results,
            }
            if cfg.lr_sched == 'plateau':
                checkpoint["lr_scheduler"] = self.scheduler.state_dict()
            torch.save(checkpoint, self._log_dir / 'last.pt')

            # wandb log
        # if epoch != -1:
            self.wbrun.log(_log_dic, step=niter)
        # Log evaluation results to file
        if self.is_main and epoch != -1:
            msg = self.stats_table.get_body() + '||' + '%10.4g' % results['loss']
            with open(self._log_dir / 'results.txt', 'a') as f:
                f.write(msg + '\n')

        return results

def main():
    TrainWrapper().main()


if __name__ == '__main__':
    main()

