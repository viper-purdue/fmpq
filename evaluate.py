import os
import json
import pickle
from tqdm import tqdm
from pathlib import Path
from collections import defaultdict, OrderedDict
import tempfile
import argparse
import time
import math
import numpy as np
import torch
from torch.utils.data import DataLoader
import torch.nn.functional as tnf
import torchvision as tv
from torch.hub import load_state_dict_from_url

import timm
import timm.utils
from pytorch_msssim import ms_ssim

from models.vae import FactorizedPrior, ScaleHyperprior, Cheng2020Anchor, model_urls, cfgs
from models.registry import get_model
from models.quantization import BIT_TYPE_DICT
from utils import ImageDataset, compute_psnr, compute_msssim, get_valloader, get_trainloader, pad, crop, compute_padding, get_model_size, calibrate, quant_stats
from compressai.zoo.pretrained import load_pretrained
from models.quantization.quant_layer import QuantModule
from models.quantization.quant_model import QuantModel


def get_object_size(obj, unit='bits'):
    assert unit == 'bits'
    pickle.dump(obj, open("bits", "wb"))
    num_bits = os.path.getsize("bits") * 8
    os.remove("bits")
        
    return num_bits

def evaluate_model(model, model_name, args, KODAK=False, CLIC=False, TECNICK=False):
    if KODAK or CLIC or TECNICK:
        test_transform = tv.transforms.Compose([tv.transforms.ToTensor()])
    else:
        test_transform = tv.transforms.Compose([
            tv.transforms.RandomResizedCrop(args.img_size),
            tv.transforms.ToTensor(),
        ])
    if KODAK:
        testset = ImageDataset(os.path.join(args.data_root, 'kodak'), transform=test_transform) 
    elif CLIC:
        testset = ImageDataset(os.path.join(args.data_root, 'clic41'), transform=test_transform) 
    elif TECNICK:
        testset = ImageDataset(os.path.join(args.data_root, 'tecnick'), transform=test_transform) 
    else:
        testset = ImageDataset(os.path.join(args.data_root, 'coco/images/val2017'), transform=test_transform)  
        
    testloader = DataLoader(
        testset, batch_size=args.batch_size, shuffle=False, num_workers=args.workers,
        pin_memory=True, drop_last=True
    )

    device = next(model.parameters()).device

    pbar = tqdm(testloader)
    stats_accumulate = defaultdict(float)
    for im in pbar:
        im = im.to(device=device)
        nB, imC, imH, imW = im.shape
                
        if model_name == "cheng_anchor" and not (CLIC or TECNICK):
            p = 256  # maximum 6 strides of 2, and window size 4 for the smallest latent fmap: 4*2^6=256
            im = pad(im, p)
        
        if CLIC or TECNICK:
            padx, unpad = compute_padding(imH, imW, min_div=2**6)
            im_padded = tnf.pad(im, padx, mode="constant", value=0)  

        # sender side: 
        if args.real_bpp:
            if CLIC or TECNICK:
                compressed_obj = model.compress(im_padded)
            else:
                compressed_obj = model.compress(im)   
            # if args.quant_model:
            #     if CLIC or TECNICK:
            #         compressed_obj = model.model.compress(im_padded)
            #     else:
            #         compressed_obj = model.model.compress(im)   
            # else:
            #     if CLIC or TECNICK:
            #         compressed_obj = model.compress(im_padded)
            #     else:
            #         compressed_obj = model.compress(im) 
            num_bits = get_object_size((compressed_obj["strings"][0], compressed_obj["strings"][1], compressed_obj["shape"]))
            bpp = num_bits / float(imH * imW)
            
            # receiver side: 
            rec_im = model.decompress(compressed_obj)["x_hat"]            
            # if args.quant_model:
            #     rec_im = model.model.decompress(compressed_obj)["x_hat"]
            # else:
            #     rec_im = model.decompress(compressed_obj)["x_hat"]
        else:
            if CLIC or TECNICK:
                out = model(im_padded)
            else:
                out = model(im)
            rec_im = out["x_hat"]
            bpp = sum(
                    (-1.0 * torch.log2(likelihood).sum(dim=(1, 2, 3)) / float(imH * imW))
                    for likelihood in out['likelihoods'].values()).item()
        
        if CLIC or TECNICK:
            rec_im = tnf.pad(rec_im, unpad)
        
        if model_name == "cheng_anchor" and not (CLIC or TECNICK):
            rec_im = crop(rec_im, (imH, imW))
            rec_im.clamp_(0, 1)

        stats_accumulate['count'] += float(nB)
        stats = {
            'PSNR': compute_psnr(im, rec_im),
            'MS-SSIM': compute_msssim(im, rec_im),
            'bpp': bpp
        }
        for k, v in stats.items():
            stats_accumulate[k] += float(v)

        # logging
        _cnt = stats_accumulate['count']
        msg = ', '.join([f'{k}={v/_cnt:.4g}' for k,v in stats_accumulate.items() if (k != 'count')])
        pbar.set_description(msg)
    pbar.close()

    # compute total statistics and return
    total_count = stats_accumulate.pop('count')
    results = {k: v/total_count for k,v in stats_accumulate.items()}
    return results


def evaluate_all_bit_rate(model_name, args, KODAK=False, CLIC=False, TECNICK=False):
    device = torch.device('cpu')
    # device = torch.device('cuda', 6)
    if not args.quant_model:
        checkpt_name = model_name
    else:
        checkpt_name = args.checkpoint
    checkpoint_root = Path(f'checkpoints/{checkpt_name}')
    
    if KODAK:
        os.makedirs('results_kodak', exist_ok=True)
        if args.real_bpp:
            save_json_path = Path(f'results_kodak/{checkpt_name}_kodak.json')
        else:
            save_json_path = Path(f'results_kodak/{checkpt_name}_entropy_bpp_kodak.json')
    elif CLIC:
        os.makedirs('results_clic', exist_ok=True)
        if args.real_bpp:
            save_json_path = Path(f'results_clic/{checkpt_name}_clic.json')
        else:
            save_json_path = Path(f'results_clic/{checkpt_name}_entropy_bpp_clic.json')
    elif TECNICK:
        os.makedirs('results_tecnick', exist_ok=True)
        if args.real_bpp:
            save_json_path = Path(f'results_tecnick/{checkpt_name}_tecnick.json')
        else:
            save_json_path = Path(f'results_tecnick/{checkpt_name}_entropy_bpp_tecnick.json')
    else:
        save_json_path = Path(f'results/{checkpt_name}.json')

    if save_json_path.is_file():
        print(f'==== Warning: {save_json_path} already exists. Will overwrite it! ====')
    else:
        print(f'Will save results to {save_json_path} ...')

    if args.quant_model or model_name=="mean_scale_hyperprior":
        checkpoint_paths = list(checkpoint_root.rglob('*.pt'))
        checkpoint_paths.sort()
        print(f'Found {len(checkpoint_paths)} checkpoints in {checkpoint_root}. Evaluating them ...')

    results_of_all_models = defaultdict(list)
    
    model_size_path = f'model_sizes/{checkpt_name}.json'
    FP_model_sizes = 0
    FPQ_model_sizes = 0
    MPQ_model_sizes = 0
    
    for path_idx, (hyper_params, url) in enumerate(model_urls[model_name]["mse"].items()):
        if KODAK or CLIC or TECNICK:
            args.batch_size = 1
        
        wq_params = {'scale_method': "minmax", 'bit_type': BIT_TYPE_DICT['uint8'], 'calibration_mode': "channel_wise", 'quantizer_type': "scaled-lsq"}
        aq_params = {'scale_method': "minmax", 'bit_type': BIT_TYPE_DICT[args.aquant_bits], 'calibration_mode': args.aquant_calibration_mode, 'quantizer_type': "scaled-lsq", 'dynamic': args.dynamic_aquant}
        
        quality, (N, M), bpp_lmb = hyper_params 
        print(f"Quality: {quality}") 
        if args.quant_model:
            model = get_model(model_name+"_quantized")(N, M, bpp_lmb, wq_params, aq_params, quality, device=device, pretrained=True)
        else:
            model = get_model(model_name)(N, M, bpp_lmb)
        model = model.to(device=device)
        model.eval()
                            
        if args.MPQ:
            with open(os.path.join(checkpoint_root, f'bit_types_{quality}.json'), 'r') as f:
                bit_type_names = json.load(f) 
            modules_to_quant = []
            module_names = []
            for name, module in model.named_modules():
                if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                    modules_to_quant.append(module)
                    module_names.append(name)
            last_module_name = module_names[-1]
            assert len(modules_to_quant) == len(bit_type_names), '--Mismatch in the number of layers to be quantized'
            for i, (module, bit_type_str) in enumerate(zip(modules_to_quant, bit_type_names.values())): 
                if module_names[i] == last_module_name:
                    module.init_MPQ(BIT_TYPE_DICT[bit_type_str], act_bit_type=BIT_TYPE_DICT['uint8'])
                else:
                    module.init_MPQ(BIT_TYPE_DICT[bit_type_str])
 
        if args.quant_model:
            DATA_DIR = Path(os.path.join(args.data_root, 'coco/images')) 
            calib_split = 'calib2017'
            calib_dir = os.path.join(DATA_DIR, calib_split)
            dataloader = get_valloader(root_dir=calib_dir, img_size=args.img_size, batch_size=32, workers=args.workers)
            calibrate(model, dataloader)
            model.set_quant_state(True, True)        
        
        if args.quant_model or model_name=="mean_scale_hyperprior":
            ckptpath = checkpoint_paths[path_idx]
            checkpoint = torch.load(ckptpath)
            model.load_state_dict(checkpoint['model'], strict=True)
            if args.quant_model:
                model.set_quant_state(True, True)
        else:
            state_dict = load_state_dict_from_url(url, progress=True)
            state_dict = load_pretrained(state_dict)
            model.load_state_dict(state_dict, strict=True)
                
        FP_model_sizes += get_model_size(model, model_type="full_precision", bit_per_module=None)  
        print(f'Full precision floating point model size: {get_model_size(model, model_type="full_precision", bit_per_module=None)} MB')     
        if args.quant_model:
            FPQ_model_sizes += get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8)
            print(f'Fixed precision quantized model size: {get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8)} MB')
            if args.MPQ:
                MPQ_model_sizes += get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()))
                print(f'Mixed precision quantized model size: {get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()))} MB')
        
        if args.verbose == 1 and args.quant_model:
            print("Quantization sanity check")
            quant_stats(model, checkpoint_root, quality)

        # ######### TEST CODE ##############################################
        for name, module in model.named_modules():
            if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                module.weight = torch.nn.Parameter(module.weight_quantizer(module.weight))
        model.set_quant_state(False, True) 
        # ######### TEST CODE ##############################################
        model.eval()
        model.update()

        results = evaluate_model(model, model_name, args, KODAK=KODAK, CLIC=CLIC, TECNICK=TECNICK)
        print(results)
        results['quality-lmb'] = (quality, bpp_lmb)
        results
        for k,v in results.items():
            results_of_all_models[k].append(v)

        with open(save_json_path, 'w') as f:
            json.dump(results_of_all_models, fp=f, indent=4)
    
    model_sizes = {}
    model_sizes['FP'] = FP_model_sizes / quality
    if args.quant_model:
        model_sizes['FPQ'] = FPQ_model_sizes / len(checkpoint_paths)
        if args.MPQ:
            model_sizes['MPQ'] = MPQ_model_sizes / len(checkpoint_paths)
        
    with open(model_size_path, 'w') as f:
        json.dump(model_sizes, fp=f, indent=4)
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model', type=str, default='scale_hyperprior')                                                                     # 'ours_n0', 'ours_n4', 'ours_n8'
    parser.add_argument('-c', '--checkpoint', type=str, default='')
    parser.add_argument('-d', '--data_root',  type=str, default='/home/jackfruit/a/hossai34/datasets')
    parser.add_argument('-i', '--img_size', type=int, default=256)
    parser.add_argument('-b', '--batch_size', type=int, default=1)
    parser.add_argument('-w', '--workers',    type=int, default=1)
    parser.add_argument('--kodak', action=argparse.BooleanOptionalAction, default=False)    
    parser.add_argument('--clic', action=argparse.BooleanOptionalAction, default=False)                                
    parser.add_argument('--tecnick', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--quant_model', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--dynamic_aquant', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--MPQ', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--aquant_calibration_mode', type=str, default="channel_wise", choices=['layer_wise', 'channel_wise'])
    parser.add_argument('--aquant_bits', type=str, default="uint8")
    parser.add_argument('--real_bpp', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--verbose', type=int, default=0, choices=[0, 1])
    args = parser.parse_args()

    # torch.set_num_threads(1)
    # for model_name in args.models:
    model_name = args.model
    evaluate_all_bit_rate(model_name, args, KODAK=args.kodak, CLIC=args.clic, TECNICK=args.tecnick)
    print()


if __name__ == '__main__':
    main()
