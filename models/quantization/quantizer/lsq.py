import torch
import torch.nn as nn

from .base import BaseQuantizer


def grad_scale(x, scale):
    y = x
    y_grad = x * scale
    return (y - y_grad).detach() + y_grad

def round_pass(x):
    y = x.round()
    y_grad = x
    return (y - y_grad).detach() + y_grad

def round_ste(x: torch.Tensor):
    """
    Implement Straight-Through Estimator for rounding operation.
    """
    return (x.round() - x).detach() + x

def noise_pass(x):
    half = float(0.5)
    noise = torch.empty_like(x).uniform_(-half, half)
    x = x + noise
    return x

class LsqQuantizer(BaseQuantizer):
    
    def __init__(self, bit_type, observer, if_tconv, is_act):
        super(LsqQuantizer, self).__init__(bit_type, observer, if_tconv, is_act)
        self.scale = nn.Parameter(torch.ones((1)))
        self.zero_point = nn.Parameter(torch.ones((1)))
    
    def init_from(self, x):
        range_shape = self.get_reshape_range(x)
        x = x.detach()
        if self.observer.calibration_mode == "channel_wise":
            if self.if_tconv:
                x = x.permute(1, 0, 2, 3)
            temp_scale = x.abs().mean(dim=list(range(1, x.dim())), keepdim=True) * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
        else:
            if self.if_tconv:
                x = x.permute(1, 0, 2, 3)
            temp_scale = x.abs().mean() * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
        self.zero_point = nn.Parameter(torch.zeros_like(temp_scale))
        
    def update_quantization_params(self, *args, **kwargs):
        scale, zero_point = self.observer.get_quantization_params(
            *args, **kwargs)
        self.scale = nn.Parameter(scale)
        self.zero_point = nn.Parameter(zero_point)

    def quant(self, x, scale=None, zero_point=None, use_bias=True, STE=True):
        s_scale = self.scale
        range_shape = self.get_reshape_range(x)
        s_scale = s_scale.reshape(range_shape)        
        s_zero_point = self.zero_point
        s_zero_point = s_zero_point.reshape(range_shape)
        if use_bias:
            x = x / s_scale + s_zero_point
        else:
            self.zero_point.detach()
        if STE:
            x = round_pass(x) 
            x = torch.clamp(x, self.bit_type.lower_bound, self.bit_type.upper_bound)
        else:
            x = noise_pass(x)
        if use_bias:
            output = (x - s_zero_point) * s_scale
        else:
            output = x * s_scale
                    
        return output

    def dequantize(self, x, scale=None, zero_point=None):
        output = x
        return output