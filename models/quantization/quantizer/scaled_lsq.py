import torch
import torch.nn as nn

from .base import BaseQuantizer


def box_grad(x, low, high):
    mask = (x >= low) & (x <= high)
    out = torch.zeros_like(x)
    out[mask] = 1
    
    return out

def box_grad_weights(x, low, high):
    mask = (x >= low) & (x <= high)
    out = torch.ones_like(x) * 0.01
    out[mask] = 1
    
    return out

class ScaledRound(torch.autograd.Function):
    """
    x_in: continuous inputs within the range of [0,1]
    num_levels: number of discrete levels
    scaling_factor: backward scaling factor
    x_out: discretized version of x_in within the range of [0,1]
    """
    @staticmethod
    def forward(ctx, x: torch.Tensor, scale: torch.Tensor, zp: torch.Tensor, Qn, Qp):
        ctx._Qp = Qp
        ctx._Qn = Qn
        
        x_hat = x/scale + zp
        out = (torch.clamp(x_hat, Qn, Qp).round() - zp) * scale        
        ctx.save_for_backward(x, x_hat, scale, zp, out)

        return out
    
    @staticmethod
    def backward(ctx, g):
        Qp = ctx._Qp 
        Qn = ctx._Qn
        x, x_hat, scale, zp, out = ctx.saved_tensors
        clip_grad = box_grad(x_hat, Qn, Qp)
        clip_grad_weights = box_grad_weights(x_hat, Qn, Qp)
        dx = g * clip_grad_weights #(g - e) * clip_grad
        dscale = g * (torch.clamp(x_hat, Qn, Qp).round() - clip_grad*(x/scale) - zp)
        dzp = g * (scale*clip_grad - scale)

        return dx, dscale, dzp, None, None, None


class ScaledLsqQuantizer(BaseQuantizer):
    
    def __init__(self, bit_type, observer, if_tconv, is_act):
        super(ScaledLsqQuantizer, self).__init__(bit_type, observer, if_tconv, is_act)
        self.scale = nn.Parameter(torch.ones((1)))
        self.zero_point = nn.Parameter(torch.ones((1)))
    
    def init_from(self, x):
        range_shape = self.get_reshape_range(x)
        x = x.detach()
        if self.observer.calibration_mode == "channel_wise":
            if self.if_tconv:
                x = x.permute(1, 0, 2, 3)
            temp_scale = x.abs().mean(dim=list(range(1, x.dim())), keepdim=True) * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
        else:
            if self.if_tconv:
                x = x.permute(1, 0, 2, 3)
            temp_scale = x.abs().mean() * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
        self.zero_point = nn.Parameter(torch.zeros_like(temp_scale))
        
    def update_quantization_params(self, *args, **kwargs):
        scale, zero_point = self.observer.get_quantization_params(
            *args, **kwargs)
        self.scale = nn.Parameter(scale)
        self.zero_point = nn.Parameter(zero_point)

    def quant(self, x, scale=None, zero_point=None):
        s_scale = self.scale
        range_shape = self.get_reshape_range(x)
        s_scale = s_scale.reshape(range_shape)
        s_zero_point = self.zero_point
        s_zero_point = s_zero_point.reshape(range_shape)        
        output = ScaledRound.apply(x, s_scale, s_zero_point, self.bit_type.lower_bound, self.bit_type.upper_bound)
                
        return output

    def dequantize(self, x, scale=None, zero_point=None):
        output = x
        return output
    
