# Copyright (c) MEGVII Inc. and its affiliates. All Rights Reserved.
from .build import build_quantizer
from .dynamic import ActQuantizer
