import torch
import torch.nn as nn
import torch.nn.functional as F

from typing import Union
from compressai.layers import GDN, MaskedConv2d
from .observer import build_observer
from .quantizer import build_quantizer, ActQuantizer

    
class StraightThrough(nn.Module):
    def __init__(self, channel_num: int = 1):
        super().__init__()
        
    def forward(self, input):
        return input

def round_ste(x: torch.Tensor):
    """
    Implement Straight-Through Estimator for rounding operation.
    """
    return (x.round() - x).detach() + x


class QuantModule(nn.Module):
    r"""
        Convert module to quantmodule.
    """

    def __init__(self, org_module: Union[nn.Conv2d, nn.ConvTranspose2d, nn.LayerNorm, nn.Linear, GDN, nn.PixelShuffle, MaskedConv2d], weight_quant_params: dict = {},
                 act_quant_params: dict = {},  disable_act_quant: bool = False):
        super(QuantModule, self).__init__()

        self.if_tconv = False
        self.org_module_type = None
        
        if isinstance(org_module, MaskedConv2d):
            self.fwd_kwargs = dict(stride=org_module.stride, padding=org_module.padding,
                                   dilation=org_module.dilation, groups=org_module.groups)
            self.fwd_func = F.conv2d
            self.org_module_type = "MaskedConv2d"
        
        elif isinstance(org_module, nn.Conv2d):
            self.fwd_kwargs = dict(stride=org_module.stride, padding=org_module.padding,
                                   dilation=org_module.dilation, groups=org_module.groups)
            self.fwd_func = F.conv2d
            self.org_module_type = "Conv2d"

        elif isinstance(org_module, nn.ConvTranspose2d):
            self.fwd_kwargs = dict(stride=org_module.stride, padding=org_module.padding, 
                                   output_padding=org_module.output_padding,
                                   dilation=org_module.dilation, groups=org_module.groups)
            self.fwd_func = F.conv_transpose2d
            self.if_tconv = True
            self.org_module_type = "ConvTranspose2d"

        elif isinstance(org_module, nn.Linear):
            self.fwd_kwargs = dict()
            self.fwd_func = F.linear
            self.org_module_type = "Linear"

        elif isinstance(org_module, nn.LayerNorm):
            self.fwd_kwargs = dict(normalized_shape = org_module.normalized_shape)
            self.fwd_func = F.layer_norm
            self.org_module_type = "LayerNorm"
            self.scale_weights = False
        
        elif isinstance(org_module, GDN):
            self.fwd_kwargs = dict(inverse = org_module.inverse, gamma_reparam = org_module.gamma_reparam, 
                                   beta_reparam = org_module.beta_reparam)
            self.fwd_func = f_gdn
            self.org_module_type = "GDN"
            self.scale_weights = False
        
        elif isinstance(org_module, nn.PixelShuffle):
            self.fwd_kwargs = org_module.upscale_factor
            self.fwd_func = F.pixel_shuffle
            self.org_module_type = "PixelShuffle"
            self.scale_weights = False
        
        else:
            raise ValueError('Not supported modules: {}'.format(org_module))
        
        if isinstance(org_module, GDN):
            self.weight = org_module.gamma
            self.org_weight = org_module.gamma.data.clone()
            if org_module.beta is not None:
                self.bias = org_module.beta
                self.org_bias = org_module.beta.data.clone()
            else:
                self.bias = None
                self.org_bias = None
        elif self.org_module_type == "PixelShuffle":
            self.weight = None
            self.org_weight = None
            self.bias = None
            self.org_bias = None
        else:
            if isinstance(org_module, MaskedConv2d):
                self.mask = org_module.mask
            self.weight = org_module.weight
            self.org_weight = org_module.weight.data.clone()
            if org_module.bias is not None:
                self.bias = org_module.bias
                self.org_bias = org_module.bias.data.clone()
            else:
                self.bias = None
                self.org_bias = None
                    
        # de-activate the quantized forward default
        self.use_weight_quant = False
        self.use_act_quant = False
        self.disable_act_quant = disable_act_quant
        
        self.scale_method = weight_quant_params['scale_method']
        self.bit_type = weight_quant_params['bit_type']
        self.calibration_mode = weight_quant_params['calibration_mode']
        self.quantizer_type = weight_quant_params['quantizer_type']
        
        # needs changing so that these are not initialized for PixelShuffle
        self.weight_observer = build_observer(self.scale_method, self.bit_type, self.calibration_mode, self.if_tconv)
        self.weight_quantizer = build_quantizer(self.quantizer_type, self.bit_type, self.weight_observer, self.if_tconv)
        # self.weight_quantizer = UniformAffineQuantizer(tconv=self.if_tconv, n_bits=8, channel_wise=True, scale_method='max')
        
        self.dynamic_act = act_quant_params['dynamic']
        self.act_bit_type = act_quant_params['bit_type']
        if not self.dynamic_act:
            self.act_scale_method = act_quant_params['scale_method']
            self.act_calibration_mode = act_quant_params['calibration_mode']
            self.act_quantizer_type = act_quant_params['quantizer_type']
            if self.org_module_type == "Linear":
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, "layer_wise", if_tconv=False, is_act=True)
            else:
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, self.act_calibration_mode, if_tconv=False, is_act=True)
            self.act_quantizer = build_quantizer(self.act_quantizer_type, self.act_bit_type, self.act_observer, if_tconv=False, is_act=True)

        self.activation_function = nn.LeakyReLU(inplace=True) if self.org_module_type=="PixelShuffle" else StraightThrough()
        self.ignore_reconstruction = False

        self.extra_repr = org_module.extra_repr
        self.trained = True
        self.calibrate_weight = False
        self.calibrate_act = False
    
    def forward(self, input: torch.Tensor):
        if self.org_module_type == "PixelShuffle":
            out = self.fwd_func(input, self.fwd_kwargs)
            out = self.activation_function(out)
            return out
        if self.calibrate_weight:
            self.weight_quantizer.observer.update(self.weight)
            self.weight_quantizer.update_quantization_params()
            self.calibrate_weight = False
                    
        if self.use_weight_quant: 
            weight = self.weight_quantizer(self.weight)
            bias = self.bias
        else:
            weight = self.weight
            bias = self.bias
        
        if self.org_module_type == "LayerNorm":
            out = self.fwd_func(input, weight=weight, bias=bias, **self.fwd_kwargs)
        elif self.org_module_type == "MaskedConv2d":
            weight.data *= self.mask
            out = self.fwd_func(input, weight, bias, **self.fwd_kwargs)
        else:
            out = self.fwd_func(input, weight, bias, **self.fwd_kwargs)
        
        # disable act quantization is designed for convolution before elemental-wise operation,
        # in that case, we apply activation function and quantization after ele-wise op.
        out = self.activation_function(out)
        
        if self.calibrate_act and not self.dynamic_act:
            self.act_quantizer.observer.update(out)
            self.act_quantizer.update_quantization_params()
            
        if self.disable_act_quant:
            return out
        if self.use_act_quant and self.trained:
            if not self.dynamic_act:
                out = self.act_quantizer(out)
            else:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
        return out
    
    def init_MPQ(self, bit_type, act_bit_type=None):
        self.bit_type = bit_type
        
        self.weight_observer = build_observer(self.scale_method, self.bit_type, self.calibration_mode, self.if_tconv)
        self.weight_quantizer = build_quantizer(self.quantizer_type, self.bit_type, self.weight_observer, self.if_tconv)
        
        if act_bit_type is not None:
            self.act_bit_type = act_bit_type
            
        if not self.dynamic_act:
            if self.org_module_type == "Linear":
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, "layer_wise", if_tconv=False, is_act=True)
            else:
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, self.act_calibration_mode, if_tconv=False, is_act=True)
            self.act_quantizer = build_quantizer(self.act_quantizer_type, self.act_bit_type, self.act_observer, if_tconv=False, is_act=True)
    
    def set_quant_state(self, weight_quant: bool = False, act_quant: bool = False):
        self.use_weight_quant = weight_quant
        self.use_act_quant = act_quant
        
    def set_calibrate(self, calibrate_weight: bool = False, calibrate_act: bool = False):
        self.calibrate_weight = calibrate_weight
        self.calibrate_act = calibrate_act


def f_gdn(x, gamma, beta, inverse, gamma_reparam, beta_reparam):
    _, C, _, _ = x.size()
    gamma = gamma_reparam(gamma)
    beta = beta_reparam(beta)
    gamma = gamma.reshape(C, C, 1, 1)
    # print(gamma.device)
    # print(beta.device)
    norm = F.conv2d(x**2, gamma, beta)
    if inverse:
        norm = torch.sqrt(norm)
    else:
        norm = torch.rsqrt(norm)
    
    out = x * norm
    return out
        
        
        
        
        
        
        
        
        
        
        
        
        
