import sys
import torch
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.checkpoint as checkpoint
from timm.models.layers import DropPath, to_2tuple, trunc_normal_
from compressai.layers import GDN, MaskedConv2d
from compressai.entropy_models import EntropyBottleneck, GaussianConditional
from .block import ResidualBlockWithStride, ResidualBlockUpsample, ResidualBlock, subpel_conv3x3
from .bit_type import BIT_TYPE_DICT

# sys.path.append(".")
from .quant_layer import QuantModule, StraightThrough
from .observer import build_observer
from .quantizer import build_quantizer, ActQuantizer
from models.layers import Mlp, WindowAttention, SwinTransformerBlock, BasicLayer, RSTB


class PatchEmbed(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        x = x.flatten(2).transpose(1, 2)  # B Ph*Pw C
        return x

    def flops(self):
        flops = 0
        return flops


class PatchUnEmbed(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x, x_size):
        B, HW, C = x.shape
        x = x.transpose(1, 2).view(B, -1, x_size[0], x_size[1])
        return x

    def flops(self):
        flops = 0
        return flops


def window_partition(x, window_size):
    """
    Args:
        x: (B, H, W, C)
        window_size (int): window size

    Returns:
        windows: (num_windows*B, window_size, window_size, C)
    """
    B, H, W, C = x.shape
    x = x.view(B, H // window_size, window_size, W // window_size, window_size, C)
    windows = x.permute(0, 1, 3, 2, 4, 5).contiguous().view(-1, window_size, window_size, C)
    return windows


def window_reverse(windows, window_size, H, W):
    """
    Args:
        windows: (num_windows*B, window_size, window_size, C)
        window_size (int): Window size
        H (int): Height of image
        W (int): Width of image

    Returns:
        x: (B, H, W, C)
    """
    B = int(windows.shape[0] / (H * W / window_size / window_size))
    x = windows.view(B, H // window_size, W // window_size, window_size, window_size, -1)
    x = x.permute(0, 1, 3, 2, 4, 5).contiguous().view(B, H, W, -1)
    return x


class QAct(nn.Module):

    def __init__(self,
                 calibrate=False,
                 bit_type=BIT_TYPE_DICT['int8'],
                 calibration_mode='layer_wise',
                 scale_method='minmax',
                 quantizer_type='uniform'):
        super(QAct, self).__init__()

        self.calibrate = calibrate
        self.bit_type = bit_type
        self.calibration_mode = calibration_mode
        self.scale_method = scale_method
        self.quantizer_type = quantizer_type
        
        self.act_observer = build_observer(self.scale_method, self.bit_type, self.calibration_mode, if_tconv=False, is_act=True)
        self.act_quantizer = build_quantizer(self.quantizer_type, self.bit_type, self.act_observer, if_tconv=False, is_act=True)

    def forward(self, x):
        if self.calibrate:
            self.act_quantizer.observer.update(x)
            self.act_quantizer.update_quantization_params(x)

        x = self.act_quantizer(x)
        
        return x


class BaseQuantBlock(nn.Module):
    """
    Base implementation of block structures for all networks.
    Due to the branch architecture, we have to perform some activation function
    and quantization after the elemental-wise add operation, therefore, we
    put this part in this class.
    """
    def __init__(self, act_quant_params: dict = {}):
        super().__init__()
        self.use_weight_quant = False
        self.use_act_quant = False
        self.calibrate_weight = False
        self.calibrate_act = False
        # initialize quantizer
        self.trained = True
        
        self.dynamic_act = act_quant_params['dynamic']
        self.act_bit_type = act_quant_params['bit_type']
        if not self.dynamic_act:
            self.act_scale_method = act_quant_params['scale_method']
            self.act_calibration_mode = act_quant_params['calibration_mode']
            self.act_quantizer_type = act_quant_params['quantizer_type']

        self.activation_function = StraightThrough()

        self.ignore_reconstruction = False

    def set_quant_state(self, weight_quant: bool = False, act_quant: bool = False):
        # setting weight quantization here does not affect actual forward pass
        self.use_weight_quant = weight_quant
        self.use_act_quant = act_quant
        for m in self.modules():
            if isinstance(m, QuantModule):
                m.set_quant_state(weight_quant, act_quant)
    
    def set_calibrate(self, calibrate_weight: bool = False, calibrate_act: bool = False):
        # setting weight quantization here does not affect actual forward pass
        self.calibrate_weight = calibrate_weight
        self.calibrate_act = calibrate_act
        for m in self.modules():
            if isinstance(m, QuantModule):
                m.set_calibrate(calibrate_weight, calibrate_act)


class QuantRBWS(BaseQuantBlock):
    """
    Implementation of Quantized ResidualBlockWithStride used in Cheng2020.
    """
    def __init__(self, basic_block: ResidualBlockWithStride, weight_quant_params: dict = {}, act_quant_params: dict = {}):
        super().__init__(act_quant_params)
        self.conv1 = QuantModule(basic_block.conv1, weight_quant_params, act_quant_params, disable_act_quant=True)
        self.leaky_relu = basic_block.leaky_relu
        self.conv2 = QuantModule(basic_block.conv2, weight_quant_params, act_quant_params)
        self.gdn = QuantModule(basic_block.gdn, weight_quant_params, act_quant_params)
        if basic_block.skip is not None:
            self.skip = QuantModule(basic_block.skip, weight_quant_params, act_quant_params)
        else:
            self.skip = None
        if not self.dynamic_act:
            self.qact1 = QAct(self.calibrate_act, self.act_bit_type, self.act_calibration_mode, self.act_scale_method, self.act_quantizer_type)
            self.qact2 = QAct(self.calibrate_act, self.act_bit_type, self.act_calibration_mode, self.act_scale_method, self.act_quantizer_type)

    def forward(self, x):
        identity = x
        out = self.conv1(x)
        out = self.leaky_relu(out)
        if self.use_act_quant and self.trained:
            if self.dynamic_act:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
            else:
                out = self.qact1(out)
        out = self.conv2(out)
        out = self.gdn(out)
        if self.skip is not None:
            identity = self.skip(x)
        out += identity
        if self.use_act_quant and self.trained:
            if self.dynamic_act:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
            else:
                out = self.qact2(out)
        return out 
    

class QuantRBU(BaseQuantBlock):
    """
    Implementation of Quantized ResidualBlockUpsample used in Cheng2020.
    """
    def __init__(self, basic_block: ResidualBlockUpsample, weight_quant_params: dict = {}, act_quant_params: dict = {}):
        super().__init__(act_quant_params)
        self.subpel_conv = nn.Sequential(
            QuantModule(basic_block.subpel_conv[0], weight_quant_params, act_quant_params, disable_act_quant=True),
            basic_block.subpel_conv[1]
            )
        self.leaky_relu = basic_block.leaky_relu
        self.conv = QuantModule(basic_block.conv, weight_quant_params, act_quant_params)
        self.igdn = QuantModule(basic_block.igdn, weight_quant_params, act_quant_params)
        # self.igdn = basic_block.igdn
        self.upsample = nn.Sequential(
            QuantModule(basic_block.upsample[0], weight_quant_params, act_quant_params),
            basic_block.upsample[1]
            )
        if not self.dynamic_act:
            self.qact1 = QAct(self.calibrate_act, self.act_bit_type, self.act_calibration_mode, self.act_scale_method, self.act_quantizer_type)
            self.qact2 = QAct(self.calibrate_act, self.act_bit_type, self.act_calibration_mode, self.act_scale_method, self.act_quantizer_type)

    def forward(self, x):
        identity = x
        out = self.subpel_conv(x)
        out = self.leaky_relu(out)
        if self.use_act_quant and self.trained:
            if self.dynamic_act:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
            else:
                out = self.qact1(out)
        out = self.conv(out)
        out = self.igdn(out)
        identity = self.upsample(x)
        out += identity
        if self.use_act_quant and self.trained:
            if self.dynamic_act:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
            else:
                out = self.qact2(out)
        return out 

class QuantRB(BaseQuantBlock):
    """
    Implementation of Quantized ResidualBlock used in Cheng2020.
    """
    def __init__(self, basic_block: ResidualBlock, weight_quant_params: dict = {}, act_quant_params: dict = {}):
        super().__init__(act_quant_params)
        self.conv1 = QuantModule(basic_block.conv1, weight_quant_params, act_quant_params, disable_act_quant=True)
        self.leaky_relu = basic_block.leaky_relu
        self.conv2 = QuantModule(basic_block.conv2, weight_quant_params, act_quant_params, disable_act_quant=True)
        if basic_block.skip is not None:
            self.skip = QuantModule(basic_block.skip, weight_quant_params, act_quant_params)
        else:
            self.skip = None
        if not self.dynamic_act:
            self.qact1 = QAct(self.calibrate_act, self.act_bit_type, self.act_calibration_mode, self.act_scale_method, self.act_quantizer_type)
            self.qact2 = QAct(self.calibrate_act, self.act_bit_type, self.act_calibration_mode, self.act_scale_method, self.act_quantizer_type)
            self.qact3 = QAct(self.calibrate_act, self.act_bit_type, self.act_calibration_mode, self.act_scale_method, self.act_quantizer_type)

    def forward(self, x):
        identity = x
        out = self.conv1(x)
        out = self.leaky_relu(out)
        if self.use_act_quant and self.trained:
            if self.dynamic_act:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
            else:
                out = self.qact1(out)
        out = self.conv2(out)
        out = self.leaky_relu(out)
        if self.use_act_quant and self.trained:
            if self.dynamic_act:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
            else:
                out = self.qact2(out)
        if self.skip is not None:
            identity = self.skip(x)
        out = out + identity
        if self.use_act_quant and self.trained:
            if self.dynamic_act:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
            else:
                out = self.qact3(out)
        return out     

class QuantSC(BaseQuantBlock):
    """
    Implementation of Quantized subpel_conv3x3 used in Cheng2020.
    """
    def __init__(self, basic_block: subpel_conv3x3, weight_quant_params: dict = {}, act_quant_params: dict = {}):
        super().__init__(act_quant_params)
        self.subpel_conv = nn.Sequential(
            QuantModule(basic_block[0], weight_quant_params, act_quant_params, disable_act_quant=True),
            basic_block[1],
            nn.LeakyReLU(inplace=True),
            )

    def forward(self, x):
        return self.subpel_conv(x)   
                           

specials = {
    ResidualBlockWithStride: QuantRBWS,
    ResidualBlockUpsample: QuantRBU,
    ResidualBlock: QuantRB,
    subpel_conv3x3: QuantSC,
}
