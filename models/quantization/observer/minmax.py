# Copyright (c) MEGVII Inc. and its affiliates. All Rights Reserved.
import torch

from .base import BaseObserver


class MinmaxObserver(BaseObserver):

    def __init__(self, bit_type, calibration_mode, if_tconv, is_act):
        super(MinmaxObserver, self).__init__(bit_type, calibration_mode, if_tconv, is_act)
        self.symmetric = False

    def update(self, v):
        self.n_levels = self.bit_type.upper_bound - self.bit_type.lower_bound
        channel_wise = False
        if self.calibration_mode == "channel_wise":
            channel_wise = True
        self.scale, self.zero_point = self.init_quantization_scale(v, channel_wise)

    def get_quantization_params(self, *args, **kwargs):
        scale = self.scale
        zero_point = self.zero_point

        return scale, zero_point

    def init_quantization_scale(self, x: torch.Tensor, channel_wise: bool = False):
        delta, zero_point = None, None
        if channel_wise:
            x_clone = x.clone().detach()
            if self.if_tconv or self.is_act:
                n_channels = x_clone.shape[1]
            else:
                n_channels = x_clone.shape[0] 

            if len(x.shape) == 4:
                if self.if_tconv or self.is_act:
                    x_max = x_clone.abs().max(dim=-1)[0].max(dim=-1)[0].max(dim=0)[0]       # x_clone.reshape(x.shape[1], -1).max(dim=1)[0]
                else:
                    x_max = x_clone.abs().max(dim=-1)[0].max(dim=-1)[0].max(dim=-1)[0]      # x_clone.reshape(x.shape[0], -1).max(dim=1)[0] 
                
            elif len(x.shape) == 1:
                x_max = x_clone.abs().max()

            else: 
                x_max = x_clone.abs().max(dim=-1)[0]

            delta = x_max.clone()
            zero_point = x_max.clone()

            if len(x.shape) == 1:
                delta, zero_point = self.init_quantization_scale(x_clone, channel_wise=False)
            else:
                if self.if_tconv or self.is_act:
                    for c in range(n_channels):
                        delta[c], zero_point[c] = self.init_quantization_scale(x_clone[:, c], channel_wise=False)
                else:
                    for c in range(n_channels):
                        delta[c], zero_point[c] = self.init_quantization_scale(x_clone[c], channel_wise=False)

            if len(x.shape) == 4:
                if self.if_tconv or self.is_act:
                    delta = delta.view(1, -1, 1, 1)
                    zero_point = zero_point.view(1, -1, 1, 1)
                else:
                    delta = delta.view(-1, 1, 1, 1)
                    zero_point = zero_point.view(-1, 1, 1, 1)
            elif len(x.shape) == 1:
                delta = delta.view(-1)
                zero_point = zero_point.view(-1)
            else:
                delta = delta.view(-1, 1)
                zero_point = zero_point.view(-1, 1)
        else:
            x_min = min(x.min().item(), 0)
            x_max = max(x.max().item(), 0)

            delta = torch.tensor((x_max - x_min) / (self.n_levels - 1))
            delta = torch.max(delta, self.eps)

            zero_point = (- x_min / delta).round()
            delta = torch.tensor(delta).type_as(x)
            zero_point = torch.tensor(zero_point).type_as(x)
        
        return delta, zero_point