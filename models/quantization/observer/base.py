# Copyright (c) MEGVII Inc. and its affiliates. All Rights Reserved.
import torch


class BaseObserver:

    def __init__(self, bit_type, calibration_mode, if_tconv, is_act):
        self.bit_type = bit_type
        self.calibration_mode = calibration_mode
        self.if_tconv = if_tconv
        self.is_act = is_act
        self.max_val = None
        self.min_val = None
        # self.eps = torch.finfo(torch.float32).eps               # self.eps = 1.1920928955078125e-07
        if self.is_act:
            self.eps = torch.tensor(1e-6, dtype=torch.float32)
        else:
            self.eps = torch.tensor(1e-8, dtype=torch.float32)

    def reshape_tensor(self, v):
        if not isinstance(v, torch.Tensor):
            v = torch.tensor(v)
        v = v.detach()
        if self.if_tconv:
            v = v.reshape(v.shape[1], -1)
        elif self.is_act:
            if len(v.shape) == 4:                  
                v = v.permute(0, 2, 3, 1)
            v = v.reshape(-1, v.shape[-1])
            v = v.transpose(0, 1)
        else:
            # reshape weight tensors into 2D [out_chennls, in_channels*width*height]
            v = v.reshape(v.shape[0], -1)

        return v

    def update(self, v):
        # update self.max_val and self.min_val
        raise NotImplementedError

    def get_quantization_params(self, *args, **kwargs):
        raise NotImplementedError
