import torch

from .base import BaseObserver


class StdObserver(BaseObserver):

    def __init__(self, module_type, bit_type, calibration_mode):
        super(StdObserver, self).__init__(module_type, bit_type,
                                             calibration_mode)
        self.symmetric = self.bit_type.signed
        # self.symmetric = False

    def update(self, v):
        # if self.module_type == 'activation':
        #     print(v.shape)
        v = self.reshape_tensor(v)          # reshaped weight tensor/ reshaped activation tensor
        if v.shape[1] == 1:
            cur_max = v.max(axis=1).values      # contains the maximum value in each of the convolution filters
        # if self.module_type == 'activation':
        #     print(v.shape)
        #     print("MEAN", v.mean(dim=1))
        #     print("STD", v.std(dim=1))
        else:
            cur_max = v.mean(dim=1) + v.std(dim=1)
        # if self.module_type == 'activation':
        #     print("MAX", cur_max)
        if self.max_val is None:
            self.max_val = cur_max          # contains the maximum value in each of the convolution filters
        else:
            self.max_val = torch.max(cur_max, self.max_val)
        if v.shape[1] == 1:
            cur_min = v.min(axis=1).values
        else:
            cur_min = v.mean(dim=1) - v.std(dim=1)
        if self.min_val is None:
            self.min_val = cur_min
        else:
            self.min_val = torch.min(cur_min, self.min_val)

        if self.calibration_mode == 'layer_wise':
            # print("FIRST    ", self.max_val.shape)
            # self.max_val = self.max_val.max()
            self.max_val = v.mean() + v.std()
            # self.min_val = self.min_val.min()
            self.min_val = v.mean() - v.std()
            # print("SECOND    ", self.max_val.shape)

    def get_quantization_params(self, *args, **kwargs):
        max_val = self.max_val
        min_val = self.min_val
        # print("MAX SHAPE", max_val.shape)
        # print("MIN SHAPE", min_val.shape)
        qmax = self.bit_type.upper_bound            # 255/127
        qmin = self.bit_type.lower_bound            # 0/-128

        scale = torch.ones_like(max_val, dtype=torch.float32)
        zero_point = torch.zeros_like(max_val, dtype=torch.float32)

        if self.symmetric:
            max_val = torch.max(-min_val, max_val)
            scale = max_val / (float(qmax - qmin) / 2)
            scale.clamp_(self.eps)                                  # clamps to the lower bound of self.eps
            zero_point = torch.zeros_like(max_val, dtype=torch.float32)
            
        else:
            scale = (max_val - min_val) / float(qmax - qmin)
            # print("MAX_VALUE:   ", max_val)
            # print("MIN_VALUE:   ", min_val)
            # print("QMAX:    ", qmax)
            # print("QMIN:    ", qmin)
            # print("SCALE:   ", scale)
            scale.clamp_(self.eps)                                  # clamps to the lower bound of self.eps
            # print("SCALE:   ", scale)
            zero_point = qmin - torch.round(min_val / scale)
            # print("ZERO_POINT:  ", zero_point)
            zero_point.clamp_(qmin, qmax)
            # print("ZERO_POINT:  ", zero_point)

        return scale, zero_point
