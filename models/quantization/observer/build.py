# Copyright (c) MEGVII Inc. and its affiliates. All Rights Reserved.
from .ema import EmaObserver
from .minmax import MinmaxObserver
from .omse import OmseObserver
from .percentile import PercentileObserver
from .ptf import PtfObserver
from .std import StdObserver

str2observer = {
    'minmax': MinmaxObserver,
    'ema': EmaObserver,
    'omse': OmseObserver,
    'percentile': PercentileObserver,
    'ptf': PtfObserver,
    'std': StdObserver
}


def build_observer(scale_method, bit_type, calibration_mode, if_tconv, is_act=False):
    observer = str2observer[scale_method]
    return observer(bit_type, calibration_mode, if_tconv, is_act)

