import tempfile
import math
import numpy as np
import torch
from torch.utils.data import DataLoader
import torch.nn.functional as tnf
import torchvision as tv

import timm
import timm.utils
from pytorch_msssim import ms_ssim

from registry import get_model
# from vae import FactorizedPrior, FactorizedPriorFixed, ScaleHyperpriorFixed
from quant_vae import QuantizedScaleHyperprior


def size_uncompressed(model, bits=32):
    """
    Args:
        model (nn.model): compressed model
        bits (int, optional): Bit-precision of float model (32 or 64). Defaults to 32.
    return: size of the model in Mega Bytes
    """
    size = sum(p.numel() for p in model.parameters()) * bits / 8 / 1024 /1024
    
    return size

def size_compressed(model, quant_type, calibration_mode, bits=8):
    """
    Args:
        model (nn.model): compressed model
        quant_type (str): One of the two ["Linear", "K-Means"]
        calibration_mode (str): One of the two ["layer_wise", "channel_wise"]
        bits (int, optional): Bit-precision of compressed model. Defaults to 8.
    """
    # for name, param in model.named_parameters():
    #     print(name, param.numel(), param.shape)
    if quant_type == "Linear":
        num_quant_params = 1
    elif quant_type == "K-Means":
        num_quant_params = 2**bits
        
    if calibration_mode == "layer_wise":
            size = sum(p.numel()*bits + (num_quant_params*16) for p in model.parameters()) / 8 / 1024 /1024
    elif calibration_mode == "channel_wise":
        size = 0
        for name, param in model.named_parameters():
            if 'weight' in name:
                num_channels = param.shape[0]
                size += param.numel() * bits + (num_quant_params*16) * num_channels
            else:
                size += param.numel() * bits + (num_quant_params*16)
        
        size = size / 8 / 1024 /1024
    
    # if quant_type == "Linear":
    #     if calibration_mode == "layer_wise":
    #         size = sum(p.numel()*bits + (2*16) for p in model.parameters()) / 8 / 1024 /1024
    #     elif calibration_mode == "channel_wise":
    #         size = 0
    #         for name, param in model.named_parameters():
    #             if 'weight' in name:
    #                 num_channels = param.shape[0]
    #                 size += param.numel() * bits + (2*16) * num_channels
    #             else:
    #                 size += param.numel() * bits + (2*16)
            
    #         size = size / 8 / 1024 /1024
                    
    # if quant_type == "K-Means":
    #     if calibration_mode == "layer_wise":
    #         size = sum(p.numel()*bits + ((2**bits)*16) for p in model.parameters()) / 8 / 1024 /1024
    #     elif calibration_mode == "channel_wise":
    #         size = 0
    #         for name, param in model.named_parameters():
    #             if 'weight' in name:
    #                 num_channels = param.shape[0]
    #                 size += param.numel() * bits + ((2**bits)*16) * num_channels
    #             else:
    #                 size += param.numel() * bits + ((2**bits)*16)
            
    #         size = size / 8 / 1024 /1024
    
    return size
                

if __name__ == "__main__":
    model = get_model("quantized_scale_hyperprior")()
    uncompressed = size_uncompressed(model, bits=32)
    print(uncompressed)
    compressed = size_compressed(model, quant_type="Linear", calibration_mode="layer_wise", bits=8)
    print(compressed)    
    compressed = size_compressed(model, quant_type="Linear", calibration_mode="channel_wise", bits=8)
    print(compressed)
    compressed = size_compressed(model, quant_type="K-Means", calibration_mode="layer_wise", bits=8)
    print(compressed)    
    compressed = size_compressed(model, quant_type="K-Means", calibration_mode="channel_wise", bits=8)
    print(compressed)