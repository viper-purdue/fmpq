
# FMPQ: Flexible Mixed Precision Quantization for Learned Image Compression

## Overview

This repository contains the official PyTorch implementation of the ICME 2024 paper "Flexible Mixed Precision Quantization for Learned Image Compression" - Md Adnan Faisal Hossain, Zhihao Duan, Fengqing Zhu.

In the following project, we propose a Flexible Mixed Precision Quantization (FMPQ) method that assigns different bit-widths to different layers of the quantized network using the fractional change in rate-distortion loss as the bit-assignment criterion. We also introduce an adaptive search algorithm which reduces the time-complexity of searching for the desired distribution of quantization bit-widths given a fixed model size. We are able to achieve state-of-the-art BD-Rate performance on quantization of LIC models under similar model size constraints compared to other works on quantization of LIC models.



## Installation

Dependencies:

The following packages are utilized:

* Python == 3.9.15
* CompressAI == 1.2.4
* Numpy == 1.23.5
* Torchvision 0.14.1
* Torch == 1.13.1
* Tqdm == 4.64.1
* Wandb == 0.15.11

Clone this repo

```bash
  git clone https://github.com/{repo-name}.git
  cd FMPQ
```

Install Anaconda: https://www.anaconda.com/

Install Wandb and create an account: https://docs.wandb.ai/quickstart

To build an exact new conda environment from the ```spec-file.txt``` use the command:
```bash
conda create --name <name_of_environment> --file spec-file.txt
```

To install required packages into an existing environment using ```spec-file.txt``` use the command:
```bash
conda install --name <name_of_environment> --file spec-file.txt
``` 

Activate the environment:
```bash
conda activate <name_of_environment> 
```  
## Dataset

We used the Common Objects in Context (COCO) dataset for training. Place the downloaded dataset in the `/datasets` directory for training. An example dataset directory with example images is set up.

The dataset can be downloaded from : https://cocodataset.org/#download

For evaluation we used the KODAK, TECNICK and CLIC. Please download the test datasets from [datasets](https://drive.google.com/drive/folders/1mucntUWl0UHVXwfLo0vE8_reCAHTdNK6?usp=sharing) and place them in the `/datasets` directory before evaluation.

## Evaluation

### Generating psnr-bpp values for LIC models

The quantized and finetuned models reported in the paper need to be downloaded from [checkpoints](https://drive.google.com/drive/folders/1L3snQSvll4MM1DpnopKUWU5EReNcY880?usp=sharing) and placed in the `/checkpoints` directory. The quantized models corresponding to the different experiments can be evaluated using the `evaluate.py` script.

```bash
  python evaluate.py -m <model_name> -c <checkpoint_name> --<kodak/clicl/tecnick> --quant_model --dynamic_aquant --real_bpp
```

To evaluate the mixed-precision quantized models, pass the additional flag `--MPQ`. Passing the flag `--real_bpp` calculates the bpp using entropy coding and will lead to very long evaluation times for the `cheng_anchor` LIC model due to it's autoregressive nature. To perform comparisons by computing the entropy values instead (much faster), remove the flag `--real_bpp`.

For example, to generate the `psnr-bpp` results using the `kodak` dataset for the mixed-precision quantized `mean_scale_hyperprior` model: 

```bash
  python evaluate.py -m mean_scale_hyperprior -c mean_scale_hyperprior_quantized_MPQ --kodak --quant_model --dynamic_aquant --real_bpp
```

To evaluate the `psnr-bpp` values for the full-precision LIC models:

```bash
  python evaluate.py -m mean_scale_hyperprior --kodak --real_bpp
```

Set the `--verbose` FLAG to 1 to print out a quantization sanity check.

### Generating rate-distortion curves and BD-Rate calculation

To generate RD-curves and get the BD-Rate values for the quantized LIC models with respect to the full-precision LIC models use the script `plot-results.py`. The `bpp-psnr` values for the different LIC models from the paper can be found in `.json` files under the subdirectories of the directory `results_paper`.

Example script for plotting RD-curves of the figure `images_paper/flexibility.jpg`:

```bash
python plot-results.py --results_dir results_paper/results_flexibility --baseline_name cheng_anchor_entropy_bpp_kodak
```

The generated image can be found in the `/images` directory. The flag `--results_dir` takes in the path of the directory containing all the `.json` files with `bpp-psnr` values to be plotted. The flag `--baseline_name` takes in the name of the `.json` file (without the extension) that contains the `bpp-psnr` values of the RD-curve that is to be used as the baseline for calculating the BD-Rate. The `--xlim` and `--ylim` arguments can be changed to give the plots a better fit. 
## Training

To train fixed-precision quantized model:

```bash
  python train.py --model <model_name> --model_quality 1 --pretrained --quantize_model --dynamic_aquant --epochs 30
```

To perform distributed training using multiple GPUs (4 GPUs in example below):

```bash
  CUDA_VISIBLE_DEVICES=0,1,2,3 torchrun --nproc_per_node 4 train.py --model cheng_anchor_quantized --model_quality 1 --pretrained --quantize_model --dynamic_aquant --epochs 30 --ddp_find
```

To train mixed-precision quantized model:
```bash
  python train.py --model <model_name> --model_quality 1 --pretrained --quantize_model --MPQ --MPQ_threshold 0.01 --CR 1 --dynamic_aquant --epochs 30
```

To perform distributed training using multiple GPUs (4 GPUs in example below):
```bash
  CUDA_VISIBLE_DEVICES=4,5,6,7 torchrun --nproc_per_node 4 train.py --model <model_name> --model_quality 1 --pretrained --quantize_model --MPQ --MPQ_threshold 0.01 --CR 1 --dynamic_aquant --epochs 30 --ddp_find
```

The trained models can be found in `/runs/model-compression`. To enable wandb logging, pass argument `-wbmode online`.

The following arguments can be passed to training script `train.py`: 

|    Argument   |            Description          |                          Example choices                  |
| :------------ |   :-----:                        |  :--------------:                                                 |
| `--model`     | name of the model to be trained | `cheng_anchor` `cheng_anchor_quantized` `mean_scale_hyperprior` `mean_scale_hyperprior_quantized` | 
| `--model_quality`         | quality term defining compression level | `[1, 2, 3, 4, 5, 6]` |
| `--resume`  | resume prematurely ended training |  `<name_of_model_checkpoint_in_runs/model-compression>` |
| `--pretrained` | boolean whether to load pretrained model | pass `--pretrained` to set to `True`; defaults to `False` |
| `--quantize_model`  | boolean whether to load quantized model | set to `True` for quantized model |
| `--dynamic_aquant`  | boolean whether to quantize activations dynamically | set to `True` for dynamic activation quantization |
| `--MPQ` | boolean whether to perform mixed precision quantization | set to `True` for mixed precision quantization |
| `--MPQ_threshold` | initial value of threshold $` \alpha `$ | `0.01` used in the experiments |
| `--CR` | target compression ratio w.r.t 8-bit fixed precision quantized model | set to `1` to get $`4 \times `$ model size reduction from full-precision model|
| `--optim_params` | train network parameters, quantization parameters or both | `qparams` `netparams` `qparams_netparams` |
| `gpu_id` | id of gpu to be used for training | for system with 8 GPUs, int between `[0, 1, 2, 3, 4, 5, 6, 7]` |
| `--ddp_find` | used for locating unused network parameters | set to `True` for distributed training |


## License

[MIT](https://choosealicense.com/licenses/mit/)

